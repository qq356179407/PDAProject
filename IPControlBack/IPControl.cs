﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Net;

namespace DDASProject.ExtentLib
{
    
    public partial class IPControl : UserControl
    {
        private IPAddress _ipAddr; //IP地址
        private byte[] _ipAddStruct; //地址结构
        private int _port;  //端口

        
        public byte[] IPAddStruct
        {
            get { return _ipAddStruct; }
            set { _ipAddStruct = value; }
        }
        public int Port
        {
            get { return _port; }           
        }
        public System.Net.IPAddress IpAddr
        {
            get { return _ipAddr ?? new IPAddress(new byte[]{192,168,0,1}); }            
        }

        public IPControl()
        {
            InitializeComponent();        
            _ipAddStruct = new byte[4];
            _port = 0;
        }

        //第一段IP效验
        private void tbIP1_Validating(object sender, CancelEventArgs e)
        {
            string ip = tbIP1.Text;
            if ("" == ip)
            {
                e.Cancel = true;
                return;
            }
            try
            {
               int n = Convert.ToInt32(ip);
                if (n<0 || n>255)
                {
                    MessageBox.Show("您输入的值不能大于255或小于0");
                    e.Cancel = true;
                    return;
                }
                else
                {
                    _ipAddStruct[0] = Convert.ToByte(n);
                }
            }
            catch
            {
                MessageBox.Show("你输入的值非数字");
                e.Cancel = true;
            }            
            
        }
        //第二段IP效验
        private void tbIP2_Validating(object sender, CancelEventArgs e)
        {
            string ip = tbIP2.Text;
            if ("" == ip)
            {
                e.Cancel = true;
            }
            try
            {
                int n = Convert.ToInt32(ip);
                if (n < 0 || n > 255)
                {
                    MessageBox.Show("您输入的值不能大于255或小于0");
                    e.Cancel = true;
                    return;
                }
                else
                {
                    _ipAddStruct[1] = Convert.ToByte(n);
                }
            }
            catch
            {
                MessageBox.Show("你输入的值非数字");
                e.Cancel = true;
            }
        }
        //第三段IP效验
        private void tbIP3_Validating(object sender, CancelEventArgs e)
        {
            string ip = tbIP3.Text;
            if ("" == ip)
            {
                e.Cancel = true;
            }
            try
            {
                int n = Convert.ToInt32(ip);
                if (n < 0 || n > 255)
                {
                    MessageBox.Show("您输入的值不能大于255或小于0");
                    e.Cancel = true;
                    return;
                }
                else
                {
                    _ipAddStruct[2] = Convert.ToByte(n);
                }
            }
            catch
            {
                MessageBox.Show("你输入的值非数字");
                e.Cancel = true;
            }
        }
        //第四段IP效验
        private void tbIP4_Validating(object sender, CancelEventArgs e)
        {
            string ip = tbIP4.Text;
            if ("" == ip)
            {
                e.Cancel = true;
            }
            try
            {
                int n = Convert.ToInt32(ip);
                if (n < 0 || n > 255)
                {
                    MessageBox.Show("您输入的值不能大于255或小于0");
                    e.Cancel = true;
                    return;
                }
                else
                {
                    _ipAddStruct[3] = Convert.ToByte(n);
                }
            }
            catch
            {
                MessageBox.Show("你输入的值非数字");
                e.Cancel = true;
            }
        }

        //端口效验
        private void tbPort_Validating(object sender, CancelEventArgs e)
        {
            string port = tbPort.Text;
            if ("" == port)
            {
                e.Cancel = true;
            }
            try
            {
                int n = Convert.ToInt32(port);
                if (n < 1 || n > 65535)
                {
                    MessageBox.Show("您输入的值不能大于65535或小于1");
                    e.Cancel = true;
                    return;
                }               
            }
            catch
            {
                MessageBox.Show("你输入的值非数字");
                e.Cancel = true;
            }
        }

        private void tbIP1_TextChanged(object sender, EventArgs e)
        {
            if (tbIP1.Text.Length == 3)
            {
                tbIP2.Focus();
            }
        }

        private void tbIP2_TextChanged(object sender, EventArgs e)
        {
            if (tbIP2.Text.Length == 3)
            {
                tbIP3.Focus();
            }
        }

        private void tbIP3_TextChanged(object sender, EventArgs e)
        {
            if (tbIP3.Text.Length == 3)
            {
                tbIP4.Focus();
            }
        }

        private void tbIP4_TextChanged(object sender, EventArgs e)
        {
            if (tbIP4.Text.Length == 3)
            {
                tbPort.Focus();
            }
        }

        private void tbIP4_Validated(object sender, EventArgs e)
        {
            _ipAddr = new IPAddress(_ipAddStruct);
        }



        private void tbIP1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '.')
            {
                e.Handled = true;
                tbIP2.Focus();
            }
        }

        private void tbIP2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '.')
            {
                e.Handled = true;
                tbIP3.Focus();
            }
        }

        private void tbIP3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '.')
            {
                e.Handled = true;
                tbIP4.Focus();
            }
        }

        private void tbIP4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '.')
            {
                e.Handled = true;
                tbPort.Focus();
            }
        }

        private void tbPort_Validated(object sender, EventArgs e)
        {
            _port = Convert.ToInt32(tbPort.Text);
        }

    }
}
