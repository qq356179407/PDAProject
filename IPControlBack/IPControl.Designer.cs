﻿namespace DDASProject.ExtentLib
{
    partial class IPControl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.tbIP1 = new System.Windows.Forms.TextBox();
            this.tbIP2 = new System.Windows.Forms.TextBox();
            this.tbIP3 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbIP4 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbPort = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // tbIP1
            // 
            this.tbIP1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbIP1.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.tbIP1.Location = new System.Drawing.Point(1, 1);
            this.tbIP1.MaxLength = 3;
            this.tbIP1.Name = "tbIP1";
            this.tbIP1.Size = new System.Drawing.Size(25, 23);
            this.tbIP1.TabIndex = 0;
            this.tbIP1.Text = "192";
            this.tbIP1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbIP1.TextChanged += new System.EventHandler(this.tbIP1_TextChanged);
            this.tbIP1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbIP1_KeyPress);
            this.tbIP1.Validating += new System.ComponentModel.CancelEventHandler(this.tbIP1_Validating);
            // 
            // tbIP2
            // 
            this.tbIP2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbIP2.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.tbIP2.Location = new System.Drawing.Point(39, 1);
            this.tbIP2.MaxLength = 3;
            this.tbIP2.Name = "tbIP2";
            this.tbIP2.Size = new System.Drawing.Size(25, 23);
            this.tbIP2.TabIndex = 1;
            this.tbIP2.Text = "168";
            this.tbIP2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbIP2.TextChanged += new System.EventHandler(this.tbIP2_TextChanged);
            this.tbIP2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbIP2_KeyPress);
            this.tbIP2.Validating += new System.ComponentModel.CancelEventHandler(this.tbIP2_Validating);
            // 
            // tbIP3
            // 
            this.tbIP3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbIP3.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.tbIP3.Location = new System.Drawing.Point(77, 1);
            this.tbIP3.MaxLength = 3;
            this.tbIP3.Name = "tbIP3";
            this.tbIP3.Size = new System.Drawing.Size(25, 23);
            this.tbIP3.TabIndex = 2;
            this.tbIP3.Text = "0";
            this.tbIP3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbIP3.TextChanged += new System.EventHandler(this.tbIP3_TextChanged);
            this.tbIP3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbIP3_KeyPress);
            this.tbIP3.Validating += new System.ComponentModel.CancelEventHandler(this.tbIP3_Validating);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(28, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(10, 27);
            this.label1.Text = ".";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(65, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(10, 27);
            this.label2.Text = ".";
            // 
            // tbIP4
            // 
            this.tbIP4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbIP4.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.tbIP4.Location = new System.Drawing.Point(114, 1);
            this.tbIP4.MaxLength = 3;
            this.tbIP4.Name = "tbIP4";
            this.tbIP4.Size = new System.Drawing.Size(25, 23);
            this.tbIP4.TabIndex = 3;
            this.tbIP4.Text = "30";
            this.tbIP4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tbIP4.TextChanged += new System.EventHandler(this.tbIP4_TextChanged);
            this.tbIP4.Validated += new System.EventHandler(this.tbIP4_Validated);
            this.tbIP4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbIP4_KeyPress);
            this.tbIP4.Validating += new System.ComponentModel.CancelEventHandler(this.tbIP4_Validating);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(102, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(10, 27);
            this.label3.Text = ".";
            // 
            // tbPort
            // 
            this.tbPort.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbPort.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.tbPort.Location = new System.Drawing.Point(150, 1);
            this.tbPort.MaxLength = 5;
            this.tbPort.Name = "tbPort";
            this.tbPort.Size = new System.Drawing.Size(38, 23);
            this.tbPort.TabIndex = 4;
            this.tbPort.Text = "6101";
            this.tbPort.Validated += new System.EventHandler(this.tbPort_Validated);
            this.tbPort.Validating += new System.ComponentModel.CancelEventHandler(this.tbPort_Validating);
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.SystemColors.HighlightText;
            this.label4.Location = new System.Drawing.Point(140, 2);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(10, 27);
            this.label4.Text = ":";
            // 
            // IPControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbPort);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbIP4);
            this.Controls.Add(this.tbIP3);
            this.Controls.Add(this.tbIP2);
            this.Controls.Add(this.tbIP1);
            this.Name = "IPControl";
            this.Size = new System.Drawing.Size(188, 23);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox tbIP1;
        private System.Windows.Forms.TextBox tbIP2;
        private System.Windows.Forms.TextBox tbIP3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbIP4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbPort;
        private System.Windows.Forms.Label label4;
    }
}
