﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
using System.Threading;
using System.Drawing;

namespace Screenshot
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [MTAThread]
        static void Main(string[] args)
        {
            /************************************************************************/
            /*                       参数说明 
             * {}表示必选参数
             * []表示可选参数
             * /s : 静默模式 不显示窗体,如果选择静默模式下面参数才生效  
             * /p {path} :指定图片保存路径与文件名
             * /f {[png][jpg][bmp]} :图片保存格式,不指定格式默认保存为png格式
             * 例: 静默模式;保存文件名为 \screenshot\test.jgp;保存格式为jpg;
             *    screen.exe /s /p "\screenshot\test.jgp" /f "jpg" 
             */
            /************************************************************************/
            if (!Directory.Exists(@"\screenshot\"))
            {
                try
                {
                    Directory.CreateDirectory(@"\screenshot\");
                }
                catch (System.Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }                
            }
            bool isSilent = false;
            string fileName = string.Empty;
            string imgFmt = "png";  //保存格式
            ImageFormat png = ImageFormat.Png;
            if (args.Length > 0)
            {
                try
                {
                    for (int i = 0; i < args.Length; i++)
                    {
                        if (string.IsNullOrEmpty(args[i]) || " " == args[i] )
                        {
                            continue;
                        }
                        switch (args[i])
                        {                            
                            case "/s":
                                isSilent = true;
                                break;
                            case "/p":
                                fileName = args[i + 1];
                                i++;
                                break;
                            case "/f":
                                imgFmt = args[i + 1];
                                i++;
                                break;
                            default:
                                Console.WriteLine("参数错误");
                                break;
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                
                if (isSilent)
                {
                    if (string.IsNullOrEmpty(fileName))
                    {
                        fileName = @"\screenshot\" + DateTime.Now.ToString("yyMMdd-HHmmss") + "." +imgFmt;
                    }
                    if ("bmp" == imgFmt.ToLower())
                    {
                        png = ImageFormat.Bmp;
                    }else if ("jpg" == imgFmt)
                    {
                        png = ImageFormat.Jpeg;
                    }
                    Image img = ScreenshotApi.ScreenShot(Screen.PrimaryScreen.Bounds);
                    if (null != img)
                    {
                        try
                        {
                            img.Save(fileName, png);
                        }
                        catch (System.Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }                        
                    }
                }
            }
            else
            {
                Application.Run(new Screenshot());
            }
            
        }
    }
}