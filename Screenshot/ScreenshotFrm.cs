﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.Threading;

namespace Screenshot
{
    public partial class Screenshot : Form
    {
        private Image screenImg;
        public Screenshot()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnScreenshot_Click(object sender, EventArgs e)
        {            
            if (null != screenImg)
            {
                screenImg.Dispose();
            }
            int delaySecond = (int)numDelaySecond.Value;
            this.Hide();
            Thread.Sleep(delaySecond * 1000);
            screenImg = ScreenshotApi.ScreenShot(Screen.PrimaryScreen.Bounds);
            this.Show();
            if (null == screenImg)
            {
                MessageBox.Show("截图失败", "提示");
                return;
            }
            if (chkSucceedBeep.Checked)
            {
                ScreenshotApi.MessageBeep(0);
            }

            ImageFormat bmp = ImageFormat.Bmp;

            if ("PNG" == cbSaveType.Text)
            {
                bmp = ImageFormat.Png;
            }
            else if ("JPG" == cbSaveType.Text)
            {
                bmp = ImageFormat.Jpeg;
            }
            string fileName = DateTime.Now.ToString("yyMMdd-HHmmss");
            saveFileDialog1.FileName = @"\screenshot\" + fileName + "." + cbSaveType.Text;
            if (chkSucceedBeep.Checked)
            {
                try
                {
                    screenImg.Save(saveFileDialog1.FileName, bmp);
                    statusBar1.Text = "截图成功,文件:" + saveFileDialog1.FileName;
                }
                catch (System.Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    statusBar1.Text = "截图失败";
                }
            }
            else
            {
                if (DialogResult.OK == saveFileDialog1.ShowDialog())
                {
                    try
                    {
                        screenImg.Save(saveFileDialog1.FileName, bmp);
                        statusBar1.Text = "截图成功,文件:" + saveFileDialog1.FileName;
                    }
                    catch (System.Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                        statusBar1.Text = "截图失败";
                    }
                }
            }



        }

        private void Screenshot_Load(object sender, EventArgs e)
        {
            cbSaveType.SelectedIndex = 0;
        }

        private void btnSendPicture_Click(object sender, EventArgs e)
        {

        }
    }
}