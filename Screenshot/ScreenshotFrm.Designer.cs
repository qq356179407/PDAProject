﻿namespace Screenshot
{
    partial class Screenshot
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Screenshot));
            this.cbSaveType = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.numDelaySecond = new System.Windows.Forms.NumericUpDown();
            this.chkSucceedBeep = new System.Windows.Forms.CheckBox();
            this.btnScreenshot = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.btnSendPicture = new System.Windows.Forms.Button();
            this.statusBar1 = new System.Windows.Forms.StatusBar();
            this.chkAutoSave = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // cbSaveType
            // 
            this.cbSaveType.Items.Add("PNG");
            this.cbSaveType.Items.Add("BMP");
            this.cbSaveType.Items.Add("JPG");
            this.cbSaveType.Location = new System.Drawing.Point(93, 27);
            this.cbSaveType.Name = "cbSaveType";
            this.cbSaveType.Size = new System.Drawing.Size(86, 23);
            this.cbSaveType.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(11, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 20);
            this.label1.Text = "保存格式:";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(12, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 20);
            this.label2.Text = "延时(秒):";
            // 
            // numDelaySecond
            // 
            this.numDelaySecond.Location = new System.Drawing.Point(93, 62);
            this.numDelaySecond.Name = "numDelaySecond";
            this.numDelaySecond.Size = new System.Drawing.Size(86, 24);
            this.numDelaySecond.TabIndex = 3;
            this.numDelaySecond.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // chkSucceedBeep
            // 
            this.chkSucceedBeep.Checked = true;
            this.chkSucceedBeep.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSucceedBeep.Location = new System.Drawing.Point(7, 97);
            this.chkSucceedBeep.Name = "chkSucceedBeep";
            this.chkSucceedBeep.Size = new System.Drawing.Size(104, 20);
            this.chkSucceedBeep.TabIndex = 4;
            this.chkSucceedBeep.Text = "完成提示音";
            // 
            // btnScreenshot
            // 
            this.btnScreenshot.Font = new System.Drawing.Font("Tahoma", 16F, System.Drawing.FontStyle.Bold);
            this.btnScreenshot.ForeColor = System.Drawing.Color.Red;
            this.btnScreenshot.Location = new System.Drawing.Point(11, 138);
            this.btnScreenshot.Name = "btnScreenshot";
            this.btnScreenshot.Size = new System.Drawing.Size(88, 60);
            this.btnScreenshot.TabIndex = 5;
            this.btnScreenshot.Text = "截图";
            this.btnScreenshot.Click += new System.EventHandler(this.btnScreenshot_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(161, 225);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(72, 20);
            this.btnClose.TabIndex = 5;
            this.btnClose.Text = "关闭";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = "BMP|*.bmp|JPG|*.jpg|PNG|*.png";
            this.saveFileDialog1.InitialDirectory = "\\screenshot\\";
            // 
            // btnSendPicture
            // 
            this.btnSendPicture.Location = new System.Drawing.Point(161, 199);
            this.btnSendPicture.Name = "btnSendPicture";
            this.btnSendPicture.Size = new System.Drawing.Size(72, 20);
            this.btnSendPicture.TabIndex = 5;
            this.btnSendPicture.Text = "发送截图";
            this.btnSendPicture.Click += new System.EventHandler(this.btnSendPicture_Click);
            // 
            // statusBar1
            // 
            this.statusBar1.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.statusBar1.Location = new System.Drawing.Point(0, 251);
            this.statusBar1.Name = "statusBar1";
            this.statusBar1.Size = new System.Drawing.Size(238, 24);
            this.statusBar1.Text = "状态";
            // 
            // chkAutoSave
            // 
            this.chkAutoSave.Checked = true;
            this.chkAutoSave.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAutoSave.Location = new System.Drawing.Point(117, 97);
            this.chkAutoSave.Name = "chkAutoSave";
            this.chkAutoSave.Size = new System.Drawing.Size(104, 20);
            this.chkAutoSave.TabIndex = 4;
            this.chkAutoSave.Text = "自动保存";
            // 
            // Screenshot
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(238, 275);
            this.Controls.Add(this.statusBar1);
            this.Controls.Add(this.btnSendPicture);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnScreenshot);
            this.Controls.Add(this.chkAutoSave);
            this.Controls.Add(this.chkSucceedBeep);
            this.Controls.Add(this.numDelaySecond);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cbSaveType);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Screenshot";
            this.Text = "截图";
            this.Load += new System.EventHandler(this.Screenshot_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cbSaveType;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown numDelaySecond;
        private System.Windows.Forms.CheckBox chkSucceedBeep;
        private System.Windows.Forms.Button btnScreenshot;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Button btnSendPicture;
        private System.Windows.Forms.StatusBar statusBar1;
        private System.Windows.Forms.CheckBox chkAutoSave;
    }
}

