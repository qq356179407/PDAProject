﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace DDASProject.CEUtility
{
    public enum AppID : int
    {        
        Unknow = 0,  /*未知*/
        Login,/*登陆窗口*/
        FunctionFrm,/*功能选择界面*/
        CaigouShouhuo = 1000, /*采购收货*/
        CaiqieWeihu,  /*裁切维护*/
        CangkuTuiliao,  /*仓库退料*/
        CEUtility,  /*Wince公共库*/
        ChaiheBiaoqian, /*拆合标签*/
        ChengpinChuhuo, /*成品出货*/
        DiaoboZuoye, /*调拨作业*/
        ElectrolyticPaper,  /*电解纸发料*/
        GongdanFaliao,  /*工单发料*/
        HeXiang, /*合箱*/
        Pandian, /*盘点*/
        StockTaking, /*成品库存标签补打*/
        XiaotuiZuoye, /*销退作业*/
        ZashouZafa, /*杂收杂发*/
        Update = 2000, /*自动更新程序,因为自动更新程序不能自己更新自己，所以此项做保留作用*/
        CommonLib,/*公共库*/
        ExtentLib,/*扩展库*/
        MaterialAllot, /*材料调拨(新)*/
        EndproductAllot, /*成品调拨(新)*/
        PrintBarCode,  /*成品标签补打*/
        ProductMerge, /*核箱*/
        FTPClient   /*FTP客户端*/
    }
   
}
