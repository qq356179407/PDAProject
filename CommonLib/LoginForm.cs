﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace PDAProject
{
    public partial class LoginForm : PDAProject.BaseForm
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {
            base.statusBar.Text = "";
            base.statusBar.Visible = true;
        }

        private void tmrNow_Tick(object sender, EventArgs e)
        {
            base.statusBar.Text = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
        }

        private void btnConfirm_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbUserID.Text) || string.IsNullOrEmpty(tbPassword.Text))
            {
                MessageBox.Show("工号或密码不能为空", "警告");
                return;
            }
            this.DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}

