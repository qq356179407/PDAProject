﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
namespace DDASProject.WebService
{
    public class Error
    {
        public static string GetErrorMsg(ErrorCode err)
        {
            string errMsg = string.Empty;
            switch (err)
            {
                case ErrorCode.ERR_NONE:
                    errMsg = string.Empty;
                    break;
                case ErrorCode.ERR_BARCODE_NOFOUND:
                    errMsg = "无效条码";
                    break;
                case ErrorCode.ERR_QUANTITY_NOTMATCH:
                    errMsg = "数量不匹配";
                    break;
                case ErrorCode.ERR_DEPOT_NOTMATCH:
                    errMsg = "仓库不匹配";
                    break;
                case ErrorCode.ERR_LOC_NOTMATCH:
                    errMsg = "储位不匹配";
                    break;
                case ErrorCode.ERR_LOTNO_NOTMATCH:
                    errMsg = "批号不匹配";
                    break;
                case ErrorCode.ERR_MATERIAL_NOTMATCH:
                    errMsg = "料号不匹配";
                    break;
                case ErrorCode.ERR_SERIALNO_NOTMATCH:
                    errMsg = "无效条码,流水号不存在";
                    break;
                case ErrorCode.ERR_BOXNO_NOTMATCH:
                    errMsg = "无效条码,箱号不匹配";
                    break;
                case ErrorCode.ERR_BILLCODE_NOTMATCH:
                    errMsg = "单据类型不匹配";
                    break;
                case ErrorCode.ERR_BARCODE_INVAILD:
                    errMsg = "无效条码";
                    break;
                case ErrorCode.ERR_QUANTITY_OVER:
                    errMsg = "库存不足";
                    break;
                case ErrorCode.ERR_CREATEDOCHEADER_FAILD:
                    errMsg = "创建单头失败";
                    break;
                case ErrorCode.ERR_CREATEDOCBODY_FAILD:
                    errMsg = "创建单身失败";
                    break;
                case ErrorCode.ERR_POSTACCOUNT_FAILD:
                    errMsg = "过账失败";
                    break;
                case ErrorCode.ERR_USERID_NULL:
                    errMsg = "用户工号不能为空";
                    break;
                case ErrorCode.ERR_WORKNO_NOFOUND:
                    errMsg = "工单号不存在";
                    break;
                case ErrorCode.ERR_STORE_NOISSUE:
                    errMsg = "未发料";
                    break;
                default:
                    errMsg = "未定义错误";
                    break;
            }
            return errMsg;
        }
    }
    public enum ErrorCode: int
    {
        ERR_NONE = 0,
        ERR_BARCODE_NOFOUND,
        ERR_QUANTITY_NOTMATCH,
        ERR_DEPOT_NOTMATCH,
        ERR_LOC_NOTMATCH,
        ERR_LOTNO_NOTMATCH,
        ERR_MATERIAL_NOTMATCH,
        ERR_SERIALNO_NOTMATCH,
        ERR_BOXNO_NOTMATCH,
        ERR_BILLCODE_NOTMATCH,
        ERR_BARCODE_INVAILD,
        ERR_QUANTITY_OVER,
        ERR_CREATEDOCHEADER_FAILD,
        ERR_CREATEDOCBODY_FAILD,
        ERR_POSTACCOUNT_FAILD,
        ERR_USERID_NULL,
        ERR_WORKNO_NOFOUND,
        ERR_STORE_NOISSUE
    }    
}