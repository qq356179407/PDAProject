﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Net;
using System.IO;
using System.Text;


namespace DDASProject.WebService
{
    /// <summary>
    /// 成品调拨审核过账
    /// </summary>
    public class CheckTransBill : IDisposable
    {
        private TIPTOPServiceGateWay service = null;

        /// <summary>
        /// get local ip address
        /// </summary>
        /// <returns>local ip address</returns>
        private string GetIP()
        {
            IPHostEntry ipHost = Dns.GetHostEntry(Dns.GetHostName());
            IPAddress ipAddr = ipHost.AddressList[0];
            return ipAddr.ToString();
        }

        /// <summary>
        /// get the request message
        /// </summary>
        /// <param name="userCode">login name</param>
        /// <param name="billCode">transBillCode</param>
        /// <returns>request message</returns>
        private string GetRequest(string userCode, string billCode)
        {
            StringBuilder request = new StringBuilder();
            request.Append("<Request><Access><Authentication user=\"")
                .Append(userCode)
                .Append("\" password=\"\" />")
                .Append("<Connection application=\"MT\" source=\"")
                .Append(GetIP())
                .Append("\"  />")
                .Append("<Organization name=\"GZELITE\" /><Locale language=\"zh_cn\" /></Access>")
                .Append("<RequestContent><Parameter><Record><Field name=\"imm01\" value=\"")
                .Append(billCode)
                .Append("\"/>")
                .Append("</Record></Parameter></RequestContent></Request>");

            return request.ToString();
        }

        /// <summary>
        /// get the response message
        /// </summary>
        /// <param name="userCode">login name</param>
        /// <param name="billCode">transBillCode</param>
        /// <returns>return the response message</returns>
        private string GetResponse(string userCode, string billCode)
        {
            if (service == null)
            {
                service = new TIPTOPServiceGateWay();
            }
            string request = GetRequest(userCode, billCode);
            string response = service.confirmdiaobo(request);

            return response;
        }

        /// <summary>
        /// return code and description to customer
        /// </summary>
        /// <param name="userCode">login name</param>
        /// <param name="billCode">transBillCode</param>
        /// <returns>which contains code and description</returns>
        public string[] PostingAccounts(string userCode, string billCode)
        {
            string[] resList = new string[2];
            try
            {
                string response = GetResponse(userCode, billCode);
                int index1 = response.IndexOf("code=\"");
                int index12 = response.IndexOf("\" sqlcode");

                int index2 = response.IndexOf("description=\"");
                int index22 = response.IndexOf("\"/>");

                string code = response.Substring(index1 + 6, index12 - index1 - 6);
                string description = response.Substring(index2 + 13, index22 - index2 - 13);
                resList[0] = code;
                resList[1] = description;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }    
            return resList;
        }

        #region IDisposable 成员
        /// <summary>
        /// relase the resources
        /// </summary>
        public void Dispose()
        {
           
        }

        #endregion
    }
}
