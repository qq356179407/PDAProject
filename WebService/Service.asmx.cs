﻿/********************************************************************
	created:	2014/01/14
	created:	14:1:2014   10:12
	filename: 	G:\Project\PDAProject\WebService\Service.asmx.cs
	file path:	G:\Project\PDAProject\WebService
	file base:	Service.asmx
	file ext:	cs
	author:		Murry
	Remark:     如需大数据量传输请加入[CompressionSoapExtension]属性
	purpose:	Webservice project    
*********************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Configuration;
using Oracle.DataAccess.Client;
using NLog;
using System.Data;
using DDASProject.Database;
using System.Text;

namespace DDASProject.WebService
{
    /// <summary>
    /// BarCodeService 的摘要说明
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // 若要允许使用 ASP.NET AJAX 从脚本中调用此 Web 服务，请取消对下行的注释。
    // [System.Web.Script.Services.ScriptService] 
    public class BarCodeService : System.Web.Services.WebService
    {
        #region 属性与变量

        private string _ERPConnString = ConfigurationManager.ConnectionStrings["ERPConnString"].ConnectionString;
        private string _MESConnString = ConfigurationManager.ConnectionStrings["MESConnString"].ConnectionString;
        private static Logger logger = LogManager.GetCurrentClassLogger(); //日志记录器
        private OracleConnection _ERPConn; //ERP数据库连接
        private OracleConnection _MESConn; //条码数据库连接

        public Oracle.DataAccess.Client.OracleConnection ERPConn
        {
            get
            {
                if (string.IsNullOrEmpty(_ERPConnString))
                {
                    _ERPConnString = "Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.0.73)(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=topprod)));User Id=gzelite;Password=gz950;";
                }
                if (null == _ERPConn)
                {
                    _ERPConn = new OracleConnection(_ERPConnString);
                }
                return _ERPConn;
            }
            set { _ERPConn = value; }
        }

        public Oracle.DataAccess.Client.OracleConnection MESConn
        {
            get
            {
                if (string.IsNullOrEmpty(_MESConnString))
                {
                    _MESConnString = "Data Source=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=192.168.0.17)(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=mespro)));User Id=mespro;Password=mespro;";
                }
                if (null == _MESConn)
                {
                    _MESConn = new OracleConnection(_MESConnString);
                }
                return _MESConn;
            }
            set { _MESConn = value; }
        }
        #endregion

        #region 方法

        /// <summary>记录发生异常时的SQL信息</summary>
        /// <param name="log">异常消息</param>
        /// <param name="commandParameters">OracleParameter对象</param>
        private static void SQLLog(string log, params OracleParameter[] commandParameters)
        {
            StringBuilder sb = new StringBuilder();
            if (commandParameters != null)
            {
                int s = commandParameters.Length;
                for (int i = 0; i < s; i++)
                {
                    sb.Append(":");
                    sb.Append(i + 1);
                    sb.Append("=");
                    if (commandParameters[i] != null && commandParameters[i].Value != null)
                        sb.Append(commandParameters[i].Value.ToString());
                    else
                        sb.Append("null");
                    sb.Append("; ");
                }
            }
            if (sb.Length > 0) 
                logger.Error (log + sb.ToString());
            else
                logger.Error(log);
        }
        

        /// <summary>
        /// 获取当前年份中的最后一位数字
        /// </summary>
        /// <returns></returns>
        private string GetYearLastByte()
        {
            string yearLastByte = "";
            try
            {
                yearLastByte = System.DateTime.Now.Year.ToString().Substring(3, 1);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return yearLastByte;
        }

        /// <summary>
        /// 获取当前年份，用一个字符表示，超过9的用字母代替，最大支持到2030年
        /// </summary>
        /// <returns>string型年份，最大支持到2030年，最小从2000开始，超出这个范围，系统单据号将出现混乱</returns>
        private string GetYear()
        {
            string year = "";
            try
            {
                int year1 = System.DateTime.Now.Year;
                if (year1 < 2010)
                {
                    year = int.Parse(year1.ToString().Substring(2, 2)).ToString();
                }
                else
                {
                    switch (year1)
                    {
                        #region-----最大支持到2030年-----
                        case 2010:
                            year = "A";
                            break;
                        case 2011:
                            year = "B";
                            break;
                        case 2012:
                            year = "C";
                            break;
                        case 2013:
                            year = "D";
                            break;
                        case 2014:
                            year = "E";
                            break;
                        case 2015:
                            year = "F";
                            break;
                        case 2016:
                            year = "G";
                            break;
                        case 2017:
                            year = "H";
                            break;
                        case 2018:
                            year = "I";
                            break;
                        case 2019:
                            year = "J";
                            break;
                        case 2020:
                            year = "K";
                            break;
                        case 2021:
                            year = "L";
                            break;
                        case 2022:
                            year = "M";
                            break;
                        case 2023:
                            year = "N";
                            break;
                        case 2024:
                            year = "O";
                            break;
                        case 2025:
                            year = "P";
                            break;
                        case 2026:
                            year = "Q";
                            break;
                        case 2027:
                            year = "R";
                            break;
                        case 2028:
                            year = "S";
                            break;
                        case 2029:
                            year = "T";
                            break;
                        case 2030:
                            year = "U";
                            break;
                        default:
                            break;
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return year;
        }

        /// <summary> 获取当前月份，超过9的用字母代替 </summary>
        /// <returns></returns>
        private string GetMonth()
        {
            string month = "";
            try
            {
                int month1 = System.DateTime.Now.Month;
                if (month1 < 10)
                {
                    month = month1.ToString();
                }
                else
                {
                    switch (month1)
                    {
                        case 10:
                            month = "A";
                            break;
                        case 11:
                            month = "B";
                            break;
                        case 12:
                            month = "C";
                            break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return month;
        }

        /// <summary> 获取服务器的年份和月份 </summary>
        /// <returns></returns>        
        private string GetYearMonth()
        {
            return GetYearLastByte() + GetMonth();
        }

        /// <summary> 解析成品条码 </summary>
        /// <param name="barcode"></param>
        /// <returns></returns>
        private stEndproduct ParseEndProductBarcode(string barcode)
        {
            string[] endProductArr = barcode.Split('&');
            return new stEndproduct(endProductArr);
        }

        /// <summary> 解析物料条码 </summary>
        /// <param name="barcode"></param>
        /// <returns></returns>
        private stMaterial ParseMaterialBarcode(string barcode)
        {
            string[] materialArr = barcode.Split('&');
            return new stMaterial(materialArr);
        }

        /// <summary> 校验材料有效性 </summary>
        /// <param name="serialNo"></param>
        /// <returns></returns>
        private ErrorCode IsMaterialBarcodeVaild(stMaterial materialInfo, decimal checkQuantity)
        {

            ErrorCode retCode = ErrorCode.ERR_NONE;
            string sql = "SELECT quantity,storageId,nvl(locId,' ') FROM jr_material_file  WHERE  serialnumber = :serialNo";
            try
            {
                OracleParameter p1 = new OracleParameter("serialNo", OracleDbType.Varchar2, 20);
                p1.Value = materialInfo.serialNo;
                OracleDataReader odr = OracleHelper.ExecuteReader(MESConn, CommandType.Text, sql, p1);
                if (null != odr && odr.HasRows)
                {
                    if (!odr.Read())
                        return ErrorCode.ERR_SERIALNO_NOTMATCH;  //流水号错误
                    decimal quantity = odr.GetDecimal(0);

                    string depotID = odr.GetString(1);
                    string locID = odr.GetString(2);
                    if (quantity != Convert.ToDecimal(materialInfo.quantity))
                    {
                        return ErrorCode.ERR_QUANTITY_NOTMATCH;  //数量不匹配
                    }
                    else if (depotID != materialInfo.depotID)
                    {
                        return ErrorCode.ERR_DEPOT_NOTMATCH;  //仓库不匹配
                    }
                    else if (locID != materialInfo.locID && string.Empty != materialInfo.locID)
                    {
                        return ErrorCode.ERR_LOC_NOTMATCH;  //储位不匹配
                    }
                }
                sql = "select img10 from img_file where img01=:p1 and img02=:p2 and img03=:p3 and img04=:p4";
                OracleParameter[] parms = new OracleParameter[]
                {
                    new OracleParameter("p1",OracleDbType.Varchar2),
                    new OracleParameter("p2",OracleDbType.Varchar2),
                    new OracleParameter("p3",OracleDbType.Varchar2),
                    new OracleParameter("p4",OracleDbType.Varchar2)                    
                };
                parms[0].Value = materialInfo.materialNo;
                parms[1].Value = materialInfo.depotID;
                parms[2].Value = materialInfo.locID;
                parms[3].Value = materialInfo.lotNo;
                odr = OracleHelper.ExecuteReader(ERPConn, CommandType.Text, sql, parms);
                if (null != odr && odr.HasRows && odr.Read())
                {
                    if (odr.GetDecimal(0) < checkQuantity)
                    {
                        retCode = ErrorCode.ERR_QUANTITY_OVER; //库存不足
                    }
                }
                else
                {
                    retCode = ErrorCode.ERR_BARCODE_NOFOUND;  //条码错误
                }
            }
            catch (System.Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
            finally
            {
                MESConn.Close();
                ERPConn.Close();
            }
            return retCode;
        }

        /// <summary> 校验成品有效性 </summary>
        /// <param name="boxNo"></param>
        /// <returns></returns>
        private ErrorCode IsEndproductBarcodeVaild(stEndproduct endproductInfo)
        {
            ErrorCode retCode = ErrorCode.ERR_NONE;
            string sql = "SELECT  quantity,storageId,locId  FROM jr_endproduct_file  WHERE casenumber = :boxNo";
            try
            {
                OracleParameter p1 = new OracleParameter("serialNo", OracleDbType.Varchar2, 20);
                p1.Value = endproductInfo.boxNo;
                OracleDataReader odr = OracleHelper.ExecuteReader(MESConn, CommandType.Text, sql, p1);
                if (null == odr || DBNull.Value.Equals(odr))
                {
                    decimal quantity = odr.GetDecimal(0);
                    string depotID = odr.GetString(1);
                    string locID = odr.GetString(2);
                    if (quantity != Convert.ToDecimal(endproductInfo.quantity))
                    {
                        retCode = ErrorCode.ERR_QUANTITY_NOTMATCH;
                    }
                    else if (depotID != endproductInfo.depotID)
                    {
                        retCode = ErrorCode.ERR_DEPOT_NOTMATCH;
                    }
                    else if (locID != endproductInfo.locID)
                    {
                        retCode = ErrorCode.ERR_LOC_NOTMATCH;
                    }

                }
            }
            catch (System.Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
            return retCode;

        }

        /// <summary> 创建调拨单号 </summary>
        /// <param name="billType">单据类型</param>

        /// <returns></returns>
        private string CreateIMM01(string billType)
        {
            string strRet = "0001";
            try
            {
                string prefix = billType + "-" + GetYearMonth();
                string sql = "select max(substr(imm01,7,4)) serial from imm_file where imm01 like '" + prefix + "%'";
                object obRet = OracleHelper.ExecuteScalar(ERPConn, sql);
                decimal dRet = 0m;
                if (null != obRet && (dRet = Convert.ToDecimal(obRet)) > 0)
                {
                    strRet = prefix + string.Format("{0:0000}", dRet);
                }
                else
                {
                    strRet = prefix + strRet;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return strRet;

        }

        /// <summary> 生成材料调拨单 </summary>
        /// <param name="docHeader">单头数据</param>
        /// <param name="docBody">单身数据</param>
        /// <param name="isPostAccount">是否过账</param>
        /// <param name="allotBillNo">调拨单号</param>
        /// <returns></returns>
        private ErrorCode CreateMaterialAllotDoc(string[] docHeader, DataTable docBody, bool isPostAccount, out string allotBillNo)
        {
            if (null == docHeader || null == docBody)
            {
                throw new ArgumentNullException("单身或单身数据不能为空");
            }
            if (docHeader.Length < 4)
            {
                throw new FormatException("单头数组长度不正确");
            }
            allotBillNo = string.Empty;
            ErrorCode retCode = ErrorCode.ERR_NONE;
            OracleTransaction erpTransation = OracleHelper.GetOracleTransaction(_ERPConnString);
            OracleTransaction mesTransation = OracleHelper.GetOracleTransaction(_MESConnString);

            try
            {
                decimal seq = GetMaterialHisSequence(); //材料历史表序列号 
                ////////////////////////插入ERP单头////////////////////////
                OracleParameter[] parms = new OracleParameter[] 
                {
                    new OracleParameter("imm01",OracleDbType.Varchar2,10),
                    new OracleParameter("imm03",OracleDbType.Varchar2,1),
                    new OracleParameter("imm04",OracleDbType.Varchar2,1),
                    new OracleParameter("imm07",OracleDbType.Decimal),
                    new OracleParameter("imm10",OracleDbType.Varchar2,1),
                    new OracleParameter("immacti",OracleDbType.Varchar2,1),
                    new OracleParameter("immuser",OracleDbType.Varchar2,10),
                    new OracleParameter("immgrup",OracleDbType.Varchar2,6)
                };
                string billNo = CreateIMM01(docHeader[0]);  //调拨单号
                parms[0].Value = billNo;  //调拨单号
                parms[1].Value = "N"; //过账否
                parms[2].Value = "N";//拨出确认
                parms[3].Value = 0;//已印次数 imm07
                parms[4].Value = docHeader[1];//资料来源 imm10
                parms[5].Value = "Y";  //资料有效码
                parms[6].Value = docHeader[2];//调拨人
                string userID = docHeader[2].ToString();
                parms[7].Value = docHeader[3];//调拨部门

                string sql = "insert into imm_file(imm01,imm02,imm03,imm04,imm07,imm10,immacti,immuser,"
                        + "immgrup,immdate) values (:imm01,trunc(sysdate),:imm03,:imm04,:imm07,:imm10,:immacti,:immuser,:immgrup,trunc(sysdate))";

                if (OracleHelper.ExecuteNonQuery(erpTransation, CommandType.Text, sql, parms) < 1)
                {
                    erpTransation.Rollback();
                    return ErrorCode.ERR_CREATEDOCHEADER_FAILD;
                }

                int nItems = 1;
                foreach (DataRow dr in docBody.Rows)
                {
                    //插入ERP单身
                    sql = "insert into imn_file(imn01,imn02,imn03,imn04,imn05,imn06,imn09,imn10,imn15,imn16,imn17,imn19,"
                            + "imn20,imn21,imn22,imn27,imn28) values(:imn01,:imn02,:imn03,:imn04,:imn05,:imn06,:imn09,:imn10,:imn15,:imn16,:imn17,:imn19,"
                            + ":imn20,:imn21,:imn22,:imn27,:imn28)";
                    OracleParameter[] parms1 = new OracleParameter[] 
                    {
                        new OracleParameter("imn01",OracleDbType.Varchar2,10),
                        new OracleParameter("imn02",OracleDbType.Decimal),
                        new OracleParameter("imn03",OracleDbType.Varchar2,20),
                        new OracleParameter("imn04",OracleDbType.Varchar2,10),
                        new OracleParameter("imn05",OracleDbType.Varchar2,10),
                        new OracleParameter("imn06",OracleDbType.Varchar2,50),
                        new OracleParameter("imn09",OracleDbType.Varchar2,4),
                        new OracleParameter("imn10",OracleDbType.Decimal),
                        new OracleParameter("imn15",OracleDbType.Varchar2,10),
                        new OracleParameter("imn16",OracleDbType.Varchar2,10),
                        new OracleParameter("imn17",OracleDbType.Varchar2,50),
                        new OracleParameter("imn19",OracleDbType.Varchar2,10),
                        new OracleParameter("imn20",OracleDbType.Varchar2,4),
                        new OracleParameter("imn21",OracleDbType.Decimal),
                        new OracleParameter("imn22",OracleDbType.Decimal),
                        new OracleParameter("imn27",OracleDbType.Varchar2,1),
                        new OracleParameter("imn28",OracleDbType.Varchar2,4)
                    };
                    parms1[0].Value = billNo;//调拨单号
                    parms1[1].Value = nItems;//项次
                    object materialID = parms1[2].Value = dr[0]; //料号                       
                    object outDepotID = parms1[3].Value = dr[10]; //调出仓
                    object outLocID = parms1[4].Value = dr[11];//调出储                        
                    object outLotNo = parms1[5].Value = dr[2];//调出批号
                    object unit = parms1[6].Value = dr[4];//库存单位
                    object quantity = parms1[7].Value = dr[3];//调出数量
                    object inDepotID = parms1[8].Value = dr[12];//调入仓
                    object inLocID = parms1[9].Value = dr[13];//调入储
                    object inLotNo = parms1[10].Value = dr[2];//调入批号
                    object supplierID = parms1[11].Value = dr[1]; //供应商编号
                    parms1[12].Value = dr[4]; //库存单位   
                    parms1[13].Value = 1m;//转换率
                    parms1[14].Value = dr[3]; //转换后数量        
                    parms1[15].Value = "N";//结案否
                    parms1[16].Value = dr[14];
                    object area = dr[5];
                    object cap = dr[6];
                    object serialNo = dr[7];
                    object billCode = dr[8];
                    object remark = dr[15];
                    OracleHelper.ExecuteNonQuery(erpTransation, CommandType.Text, sql, parms1);

                    /////////////////////////////////更新条码表/////////////////////////////////////////
                    sql = "update jr_material_file set storageId=:inDepotID,locId=:inLocID,status='1' where serialNumber=:serialNo";
                    OracleParameter p1 = new OracleParameter("inDepotID", OracleDbType.Varchar2, 20);
                    p1.Value = inDepotID;
                    OracleParameter p2 = new OracleParameter("inLocID", OracleDbType.Varchar2, 20);
                    p2.Value = inLocID;
                    OracleParameter p3 = new OracleParameter("serialNo", OracleDbType.Varchar2, 20);
                    p3.Value = serialNo;
                    OracleHelper.ExecuteNonQuery(mesTransation, CommandType.Text, sql, p1, p2, p3); //更新材料表
                    //string operateType = "7";// 0采购收货入库，1工单发料，2工单成套发料，3杂发作业，
                    // 4杂收作业，5工单退料，6仓退作业,7调拨作业，8拆合作业，
                    // 9裁切核料，10裁切维护，11裁切入库
                    sql = "insert into jr_materialhis_file (sequence ,supplierId,materialId,lotNumber,quantity,"
                            + "unit,area,capacity,serialNumber,storageId,locId,operateDocNo,status,operateType,sourceDocNo,operateDate,remarks,TOSTORAGECODE,TOLOCCODE)"
                            + "values(:seq,:supplierId,:materialId,:lotNo,:quantity,:unit,:area,:cap,:serialNo,:outDepotID,:outLocID,:sourceDocNo,"
                            + "'1','7',' ',sysdate,:remark,:inDepotID,:inLotID)";
                    OracleParameter[] parms2 = new OracleParameter[]
                    {
                        new OracleParameter("seq",OracleDbType.Decimal),
                        new OracleParameter("supplierId",OracleDbType.Varchar2,20),
                        new OracleParameter("materialId",OracleDbType.Varchar2,20),
                        new OracleParameter("lotNo",OracleDbType.Varchar2,50),
                        new OracleParameter("quantity",OracleDbType.Decimal),
                        new OracleParameter("unit",OracleDbType.Varchar2,20),
                        new OracleParameter("area",OracleDbType.Varchar2,20),
                        new OracleParameter("cap",OracleDbType.Varchar2,20),
                        new OracleParameter("serialNo",OracleDbType.Varchar2,20),
                        new OracleParameter("outDepotID",OracleDbType.Varchar2,20),
                        new OracleParameter("outLocID",OracleDbType.Varchar2,20),
                        new OracleParameter("sourceDocNo",OracleDbType.Varchar2,20),
                        new OracleParameter("remark",OracleDbType.Varchar2,40),
                        new OracleParameter("inDepotID",OracleDbType.Varchar2,20),
                        new OracleParameter("inLotID",OracleDbType.Varchar2,20)                        
                    };
                    parms2[0].Value = seq;
                    parms2[1].Value = supplierID;
                    parms2[2].Value = materialID;
                    parms2[3].Value = outLotNo;
                    parms2[4].Value = quantity;
                    parms2[5].Value = unit;
                    parms2[6].Value = area;
                    parms2[7].Value = cap;
                    parms2[8].Value = serialNo;
                    parms2[9].Value = outDepotID;
                    parms2[10].Value = outLocID;
                    parms2[11].Value = billNo;
                    parms2[12].Value = remark;
                    parms2[13].Value = inDepotID;
                    parms2[14].Value = inLocID;

                    OracleHelper.ExecuteNonQuery(mesTransation, CommandType.Text, sql, parms2); //更新材料历史记录表
                    ++nItems;
                    ++seq;
                }
                erpTransation.Commit();
                mesTransation.Commit();

                allotBillNo = billNo;
                if (isPostAccount)
                {
                    string[] ret = PostingAccounts(userID, billNo);
                    if ("0" != ret[0]) //过账失败
                    {
                        retCode = ErrorCode.ERR_POSTACCOUNT_FAILD;
                    }
                }
            }
            catch (System.Exception ex)
            {
                erpTransation.Rollback();
                mesTransation.Rollback();
                logger.Error(ex);
                retCode = ErrorCode.ERR_CREATEDOCBODY_FAILD;
                throw ex;
            }
            finally
            {
                if (null != erpTransation && null != erpTransation.Connection)
                {
                    erpTransation.Connection.Close();
                }
                if (null != mesTransation && null != mesTransation.Connection)
                {
                    mesTransation.Connection.Close();
                }
            }
            return retCode;
        }


        /// <summary> 创建成品调拨单</summary>
        /// <param name="docHeader">单头数据</param>
        /// <param name="docBody">单身数据</param>
        /// <param name="isPostAccount">是否过账</param>
        /// <param name="allotBillNo">调拨单号</param>
        /// <returns></returns>
        private ErrorCode CreateEndproductAllotDoc(string[] docHeader, DataTable docBody, bool isPostAccount, out string allotBillNo)
        {
            if (null == docHeader || null == docBody)
            {
                throw new ArgumentNullException("单身或单身数据不能为空");
            }
            if (docHeader.Length < 4)
            {
                throw new FormatException("单头数组长度不正确");
            }
            allotBillNo = string.Empty;
            return ErrorCode.ERR_NONE;
        }

        /// <summary>
        /// 获取材料历史表序列号
        /// </summary>
        /// <returns></returns>
        private decimal GetMaterialHisSequence()
        {
            decimal sequence = 1m;
            string sql = "select max(sequence) sequence from jr_materialHis_file";
            try
            {
                object objRet = OracleHelper.ExecuteScalar(MESConn, sql);
                if (null != objRet)
                {
                    sequence = Convert.ToDecimal(objRet) + 1m;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return sequence;
        }

        /// <summary>
        /// 效验电解纸条码有效性
        /// </summary>
        /// <param name="materialInfo"></param>
        /// <returns></returns>
        private ErrorCode IsElectrolyticPaperBarcodeVaild(stMaterial materialInfo)
        {
            ErrorCode retCode = ErrorCode.ERR_NONE;
            string sql = "SELECT quantity FROM jr_material_file  WHERE  serialnumber = :serialNo";
            try
            {
                OracleParameter p1 = new OracleParameter("serialNo", OracleDbType.Varchar2, 20);
                p1.Value = materialInfo.serialNo;
                OracleDataReader odr = OracleHelper.ExecuteReader(MESConn, CommandType.Text, sql, p1);
                if (null != odr && odr.HasRows)
                {
                    if (!odr.Read())
                        return ErrorCode.ERR_SERIALNO_NOTMATCH;  //流水号错误
                    decimal quantity = odr.GetDecimal(0);
                    if (quantity != Convert.ToDecimal(materialInfo.quantity))
                    {
                        return ErrorCode.ERR_QUANTITY_NOTMATCH;  //数量不匹配
                    }
                }
            }
            catch (System.Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
            finally
            {
                MESConn.Close();
                ERPConn.Close();
            }
            return retCode;

            //MessageBox.Show("无效条码,流水号不存在");

            //MessageBox.Show("过期条码，条码数量与数据库中的不匹配！");

        }

        /// <summary>
        /// 创建电解纸单号
        /// </summary>
        /// <param name="strPrefix">单别</param>
        /// <returns></returns>
        private string GetPBSequence(string strPrefix)
        {
            try
            {
                string sequenceHead = strPrefix + "-" + GetYearMonth();
                string sql = "select max(substr(tc_sfe01,7,4)) serial from tc_sfe_file where tc_sfe01 like :prefix";
                OracleParameter p1 = new OracleParameter("prefix", OracleDbType.Varchar2, 10);
                object objRet = OracleHelper.ExecuteScalar(ERPConn, sql, p1);
                string str = "0";
                if (null != objRet)
                {
                    str = objRet.ToString();
                }
                if (string.IsNullOrEmpty(str))
                {
                    str = "0";
                }
                int maxNum = Convert.ToInt32(str) + 1;
                str = sequenceHead + string.Format("{0:0000}", maxNum);
                return str;
            }
            catch (System.Exception ex)
            {
                throw (ex);
            }

        }

        private ErrorCode CreateElectrolyticPaperDoc(string[] docHeader, DataTable docBody, out string allotBillNo)
        {
            
            if (null == docHeader || null == docBody || docBody.Rows.Count <= 0)
            {
                throw new ArgumentNullException("单别或单身为空!");
            }
            if (docHeader.Length < 3)
            {
                throw new Exception("单身数组长度错误");
            }
            ErrorCode nRet = ErrorCode.ERR_NONE;
            allotBillNo = string.Empty;
            string billCode = docHeader[0];  //单别
            string userID = docHeader[1]; //用户
            string userGroup = docHeader[2]; //部门

            string strPBSequence = GetPBSequence(billCode);  //获取PB序列号
            if (string.IsNullOrEmpty(strPBSequence))
            {
                return ErrorCode.ERR_CREATEDOCHEADER_FAILD; //创建单号失败
            }
            else if (string.IsNullOrEmpty(userID))
            {
                return ErrorCode.ERR_USERID_NULL; //用户工号为空
            }
            if (string.IsNullOrEmpty(userGroup))
            {
                userGroup = GetUserGroup(userID);
            }

            //创建单头
            OracleTransaction tran = ERPConn.BeginTransaction();
            string errSql = string.Empty;
            try
            {
                //if (ERPConn.State != ConnectionState.Open)
                //{
                //    ERPConn.Open();
                //}
               
                string sql = "insert into tc_sfe_file (tc_sfe01,tc_sfeuser,tc_sfegrup,tc_sfedate,tc_sfeacti) "
                             + "values (:sfe01,:sfeuser,:sfegrup, trunc(sysdate),'Y')";
                OracleParameter[] parames = new OracleParameter[] 
                { 
                    new OracleParameter("sfe01", OracleDbType.Varchar2, 10),
                    new OracleParameter("sfeuser", OracleDbType.Varchar2, 10),
                    new OracleParameter("sfegrup", OracleDbType.Varchar2, 6)
                };
                parames[0].Value = strPBSequence;
                parames[1].Value = userID;
                parames[2].Value = userGroup;
                if (OracleHelper.ExecuteNonQuery(tran, CommandType.Text, sql, parames) < 0)
                {
                    tran.Rollback();
                    SQLLog(sql, parames);
                    return ErrorCode.ERR_CREATEDOCHEADER_FAILD; // 创建单头失败
                }

                foreach (DataRow dr in docBody.Rows)
                {
                    object item = dr[0]; //项次
                    object materialNo = dr[2]; //料号
                    object supplierNo = dr[6]; //供应商编号
                    object lotNo = dr[3];  //批号
                    object quantity = dr[4];  //数量                    
                    object unit = dr[5];  //库存单位
                    object area = dr[7];  //面积                   
                    object cap = dr[8];  //容量                   
                    object serialNo = dr[1];  //流水号
                    object printDate = dr[9];  //打印日期
                    object remark = dr[10];  //备注

                    string[] depotLoc = GetDepotLocBySerialNo(serialNo.ToString());
                    //单身
                    sql = "insert into tc_sff_file (tc_sff01,tc_sff02,tc_sff03,tc_sff91,tc_sff92,tc_sff93,tc_sff94,tc_sff99,tc_sff100 ) " +
                          "values (:sff01,:sff02,:sff03,:sff91,:sff92,:sff93,:sff94,:sff99,:sff100) ";

                    OracleParameter[] parames1 = new OracleParameter[]
                    {
                        new OracleParameter("sfe01", OracleDbType.Varchar2, 10),
                        new OracleParameter("sfe02", OracleDbType.Decimal),
                        new OracleParameter("sfe03", OracleDbType.Varchar2, 20),
                        new OracleParameter("sfe91", OracleDbType.Varchar2, 20),
                        new OracleParameter("sfe92", OracleDbType.Varchar2, 20),
                        new OracleParameter("sfe93", OracleDbType.Varchar2, 20),
                        new OracleParameter("sfe94", OracleDbType.Varchar2, 50),
                        new OracleParameter("sfe99", OracleDbType.Varchar2, 4),
                        new OracleParameter("sfe100", OracleDbType.Decimal)
                    };
                    parames1[0].Value = strPBSequence;
                    parames1[1].Value = item;
                    parames1[2].Value = serialNo;
                    parames1[3].Value = materialNo;
                    parames1[4].Value = depotLoc[0];
                    parames1[5].Value = depotLoc[1];
                    parames1[6].Value = lotNo;
                    parames1[7].Value = unit;
                    parames1[8].Value = quantity;
                    try
                    {
                        OracleHelper.ExecuteNonQuery(tran, CommandType.Text, sql, parames1);
                    }
                    catch (System.Exception ex)
                    {
                        SQLLog(sql, parames1);
                    	throw ex;
                    }
                                       
                }
                tran.Commit(); //提交事务
                allotBillNo = strPBSequence;
            }
            catch (System.Exception ex)
            {
                logger.Error(ex);
                tran.Rollback();                
            }
            finally
            {
                ERPConn.Close();
            }
            return nRet;
        }

        /// <summary> 通过流水号获取仓储</summary>
        /// <param name="serialNo"></param>
        /// <returns></returns>
        private string[] GetDepotLocBySerialNo(string serialNo)
        {
            string[] depotLoc = new string[2];
            try
            {
                string sql = "select storageId,locId from jr_material_file where serialNumber=:sn";
                OracleParameter p1 = new OracleParameter("sn", OracleDbType.Varchar2, 20);
                p1.Value = serialNo;
                OracleDataReader odr = OracleHelper.ExecuteReader(MESConn, CommandType.Text, sql, p1);
                if (odr.Read())
                {
                    depotLoc[0] = odr["storageId"].ToString();
                    depotLoc[1] = odr["locId"].ToString();
                }
            }
            catch (Exception ex)
            {
                MESConn.Close();
                throw ex;
            }
            return depotLoc;
        }

        /// <summary>
        /// 判断表是否为空
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        private bool IsTableNull(DataTable dt)
        {
            if (null == dt || null == dt.Rows || dt.Rows.Count <= 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// 判断值是否为空之
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private bool IsNullValue(object obj)
        {
            if (null == obj || DBNull.Value.Equals(obj))
            {
                return true;
            }
            return false;
        }

        /// <summary> 创建新的流水号 </summary>
        /// <param name="workOrder">工单号</param>
        /// <returns></returns>
        private string NewSerialNo(string workOrder)
        {
            if (string.IsNullOrEmpty(workOrder))
            {
                throw new ArgumentNullException("工单号不能为空");
            }
            string sn = string.Empty;
            //解析生产工单号，截取后8位，并去掉其中的横杠。如“311-0C2437”解析后得到10C2437
            try
            {
                string str = workOrder.Substring(2, 8);       //获取批号后8位“1-0C2437”            
                sn = str.Substring(0, 1) + str.Substring(2, 6);
                string sql = "select max(substr(casenumber,8,3)) from jr_endproduct_file a where productorderid  = :id";
                OracleParameter p1 = new OracleParameter("id", OracleDbType.Varchar2, 50);
                int nRet = OracleHelper.ExecuteInt32(MESConn, sql, p1);
                sn += string.Format("{0:000}", nRet);
            }
            catch (System.Exception ex)
            {            	
            	throw ex;
            }
            
            return sn;
        }
        #region 测试方法

        [WebMethod(Description = "测试,附数据压缩")]
        [CompressionSoapExtension]
        public string HelloWorld()
        {
            return "Hello World";
        }
        #endregion
        #region 公开数据库接口

        /// <summary> 执行SQL,返回DataTable </summary>
        /// <param name="sql">sql命令</param>
        /// <param name="isErp">true：连接ERP数据库 false：连接条码数据库</param>
        /// <returns></returns>
        [WebMethod(Description = "执行SQL,返回DataTable")]
        [CompressionSoapExtension]
        public DataTable GetDataTable(string sql, bool isErp)
        {
            DataTable dt = null;
            if (string.IsNullOrEmpty(sql))
            {
                throw new ArgumentNullException("sql参数不能为空");
            }
            OracleConnection conn = ERPConn;
            if (!isErp)
            {
                conn = MESConn;
            }
            try
            {

                dt = OracleHelper.ExecuteDataTable(conn, sql);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        /// <summary> 执行SQL,返回DataSet </summary>
        /// <param name="sql">sql命令</param>
        /// <param name="isErp">true：连接ERP数据库 false：连接条码数据库</param>
        /// <returns></returns>
        [WebMethod(Description = "执行SQL,返回DataSet")]
        [CompressionSoapExtension]
        public DataSet GetDataSet(string sql, bool isErp)
        {
            DataSet ds = null;
            if (string.IsNullOrEmpty(sql))
            {
                throw new ArgumentNullException("sql参数不能为空");
            }
            OracleConnection conn = ERPConn;
            if (!isErp)
            {
                conn = MESConn;
            }
            try
            {
                ds = OracleHelper.ExecuteDataSet(conn, sql);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return ds;
        }

        /// <summary> 执行SQL,返回object </summary>
        /// <param name="sql">sql命令</param>
        /// <param name="isErp">true：连接ERP数据库 false：连接条码数据库</param>
        /// <returns></returns>
        [WebMethod(Description = "执行SQL,返回object")]
        public object ExecuteScalary(string sql, bool isErp)
        {
            object objRet = null;
            if (string.IsNullOrEmpty(sql))
            {
                throw new ArgumentNullException("sql参数不能为空");
            }
            OracleConnection conn = ERPConn;
            if (!isErp)
            {
                conn = MESConn;
            }
            try
            {
                objRet = OracleHelper.ExecuteScalar(conn, sql);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return objRet;
        }

        /// <summary> 执行SQL,返回影响行数 </summary>
        /// <param name="sql">sql命令</param>
        /// <param name="isErp">true：连接ERP数据库 false：连接条码数据库</param>
        /// <returns></returns>
        [WebMethod(Description = "执行SQL,返回影响行数")]
        public int ExecuteNonQuery(string sql, bool isErp)
        {
            int nRet = 0;
            if (string.IsNullOrEmpty(sql))
            {
                throw new ArgumentNullException("sql参数不能为空");
            }
            OracleConnection conn = ERPConn;
            if (!isErp)
            {
                conn = MESConn;
            }
            try
            {
                nRet = OracleHelper.ExecuteNonQuery(conn, sql);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return nRet;
        }


        /// <summary> 执行一组SQL命令,返回影响行数 </summary>
        /// <param name="sql">sql命令</param>
        /// <param name="isErp">true：连接ERP数据库 false：连接条码数据库</param>
        /// <returns></returns>
        [WebMethod(Description = "执行一组SQL命令,返回影响行数")]
        [CompressionSoapExtension]
        public int BatExecuteNonQuery(string[] sqlList, bool isErp)
        {
            int nRet = 0;
            if (null == sqlList || 0 == sqlList.Length)
            {
                throw new ArgumentNullException("sql参数不能为空");
            }
            OracleConnection conn = ERPConn;
            if (!isErp)
            {
                conn = MESConn;
            }
            try
            {
                nRet = OracleHelper.ExecuteNonQueryWithTransaction(conn, sqlList);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return nRet;
        }

        #endregion
        /// <summary> 登陆验证 </summary>
        /// <param name="userID">工号</param>
        /// <param name="password">密码</param>
        /// <returns>true：成功 false：失败</returns>

        [WebMethod(Description = "登陆验证")]
        public bool Login(string userID, string password, out string userPartment)
        {
            bool bRet = false;
            userPartment = "";
            string sql = "select partmentcode from jr_userinfo_file where usercode=:userID and password=:password";
            try
            {
                OracleParameter p1 = new OracleParameter("userID", OracleDbType.Varchar2, 20);
                p1.Value = userID;
                OracleParameter p2 = new OracleParameter(":password", OracleDbType.Varchar2, 20);
                p2.Value = password;
                object objDepartment = OracleHelper.ExecuteScalar(MESConn, sql, p1, p2);
                if (objDepartment != null && !DBNull.Value.Equals(objDepartment))
                {
                    bRet = true;
                    userPartment = objDepartment.ToString().Trim();
                }
                else
                {
                    bRet = false;
                }
            }
            catch (System.Exception ex)
            {
                logger.Error(ex);
            }
            return bRet;
        }

        /// <summary> 根据用户ID获取部门编号 </summary>
        /// <param name="userID">用户编号</param>
        /// <returns>部门编号</returns>
        [WebMethod(Description = "根据用户ID获取部门编号")]
        public string GetUserGroup(string userID)
        {
            if (string.IsNullOrEmpty(userID))
            {
                throw new ArgumentNullException("用户名不能为空");
            }
            string str = "";
            try
            {
                string sql = "select partmentCode from  jr_userInfo_file where userCode=:userID";
                OracleParameter p1 = new OracleParameter("userID", OracleDbType.Varchar2, 20);
                p1.Value = userID;
                object obj = OracleHelper.ExecuteScalar(MESConn, sql, p1);

                if (null != obj)
                {
                    str = obj.ToString();
                }
            }
            catch (System.Exception ex)
            {
                logger.Error(ex);
            }

            return str;
        }

        /// <summary>
        /// 获取程序最新程序信息
        /// </summary>
        /// <param name="appID">程序ID</param>
        /// <returns></returns>
        [WebMethod(Description = "通过程序ID获取最新程序信息")]
        public string GetAppInfomation(int appID)
        {
            string info = "";
            try
            {
                string sql = "select APPNAME,APPVERSION,FORCEUPDATA,UPDATESERVER,DESCRIPTIONS from jr_version_file where APPID=:AppID";
                OracleParameter AppID = new OracleParameter(":AppID", OracleDbType.Int32);
                AppID.Value = appID;
                DataTable dt = OracleHelper.ExecuteDataTable(MESConn, sql, AppID);
                if (null != dt && null != dt.Rows && dt.Rows.Count > 0)
                {
                    string appName = dt.Rows[0][0].ToString(); //文件名
                    string version = dt.Rows[0][1].ToString(); //版本号
                    string bForce = dt.Rows[0][2].ToString(); //是否强制更新
                    string downloadUrl = dt.Rows[0][3].ToString(); //下载地址
                    string description = dt.Rows[0][4].ToString(); //更新内容

                    info = appName + ";" + version + ";" + bForce + ";" + downloadUrl + ";" + description;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
            return info;
        }

        /// <summary> 获取时间 </summary>
        /// <returns></returns>
        [WebMethod(Description = "获取服务器时间")]
        public DateTime GetServerTime()
        {

            return DateTime.Now;
        }

        /// <summary>获取出货通知单对应的出货单别 </summary>
        /// <param name="billCode"></param>
        /// <returns></returns>
        [WebMethod(Description = "出货通知单单别对应的出货单别")]
        public string GetShipCode(string billCode)
        {
            string sql = "SELECT tc_slp06 FROM tc_slp_file WHERE tc_slpacti='Y' AND substr(tc_slp00,0,1)='S'  AND tc_slp02= :billID";
            OracleParameter billID = new OracleParameter("billID", OracleDbType.Varchar2);
            billID.Value = billCode;

            object objRet = OracleHelper.ExecuteScalar(ERPConn, sql, billID);
            if (null != objRet)
            {
                return objRet.ToString();
            }
            return "";
        }

        /// <summary> 调拨过账</summary>
        /// <param name="userCode">用户登录账号</param>
        /// <param name="TransferBillCode">调拨单号</param>
        /// <returns></returns>
        [WebMethod(Description = "调拨过账")]
        [CompressionSoapExtension]
        public string[] PostingAccounts(string userID, string TransferBillCode)
        {
            string[] response = null;
            try
            {
                CheckTransBill checkTransBill = new CheckTransBill();
                response = checkTransBill.PostingAccounts(userID, TransferBillCode);
            }
            catch (System.Exception ex)
            {
                logger.Error(ex);
                throw (ex);
            }
            return response;
        }


        /// <summary>
        /// 条码效验
        /// 注意:如果需要同时验证仓库与储位请用&与条码内容连接
        /// </summary>
        /// <param name="barCodeType">条码类型</param>
        /// <param name="barCode">条码内容</param>
        /// <param name="checkQuantity">效验数量</param>
        /// <returns>ErrorCode枚举</returns>
        [WebMethod(Description = "条码校验")]
        public int BarcodeVaild(int barCodeType, string barCode, decimal checkQuantity)
        {
            int nRet = 0;
            switch (barCodeType)
            {
                case 0:  //成品调拨条码
                    stEndproduct endproductInfo = ParseEndProductBarcode(barCode);
                    nRet = (int)IsEndproductBarcodeVaild(endproductInfo);
                    break;
                case 1: //材料调拨条码
                    stMaterial materiaInfo = ParseMaterialBarcode(barCode);
                    nRet = (int)IsMaterialBarcodeVaild(materiaInfo, checkQuantity);
                    break;
                case 2: //电解纸条码
                    materiaInfo = ParseMaterialBarcode(barCode);
                    nRet = (int)IsElectrolyticPaperBarcodeVaild(materiaInfo);
                    break;
                case 3:
                    break;
                default:
                    break;
            }


            return nRet;
        }

        /// <summary> 获取用户授权表 </summary>
        /// <param name="userID">工号</param>
        /// <returns></returns>
        [WebMethod(Description = "获取用户授权信息")]
        [CompressionSoapExtension]
        public DataTable GetUserAuthority(string userID)
        {
            if (string.IsNullOrEmpty(userID))
            {
                throw new ArgumentNullException("用户名不能为空");
            }
            string sql = "select functioncode from jr_authority_file where userCode=:userid and haspower='1'";
            DataTable dt = null;
            try
            {
                OracleParameter p1 = new OracleParameter("userid", OracleDbType.Varchar2, 20);
                p1.Value = userID;
                dt = OracleHelper.ExecuteDataTable(MESConn, sql, p1);
            }
            catch (System.Exception ex)
            {
                logger.Error(ex);
                throw ex;
            }
            return dt;
        }

        /// <summary> 创建单据/生单 </summary>
        /// <param name="docType">单据类型 0:成品调拨,1:材料调拨,2:电解纸...后续请自行定义</param>
        /// <param name="docHeader">单头数据</param>
        /// <param name="isPostAccount">是否过账</param>
        /// <param name="docBody">单身数据</param>
        /// <returns></returns>

        [WebMethod(Description = "创建调拨单据")]
        [CompressionSoapExtension]
        public int CreateAllotDocument(int docType, string[] docHeader, DataTable docBody, bool isPostAccount, out string allotBillNo)
        {
            ErrorCode retCode = ErrorCode.ERR_NONE;
            allotBillNo = string.Empty;
            switch (docType)
            {
                case 0:
                    retCode = CreateMaterialAllotDoc(docHeader, docBody, isPostAccount, out allotBillNo);
                    break;
                case 1:
                    retCode = CreateEndproductAllotDoc(docHeader, docBody, isPostAccount, out allotBillNo);
                    break;
                case 2:
                    retCode = CreateElectrolyticPaperDoc(docHeader, docBody, out allotBillNo);
                    break;
                default:
                    break;
            }

            return (int)retCode;
        }

        /// <summary> 效验工单号是否有效 </summary>
        /// <param name="workOrder">工单号</param>
        /// <param name="info">提示信息</param>
        /// <returns></returns>
        [WebMethod(Description="工单号效验")]
        public bool WorkOrderVaild(string workOrder,out string info)
        {           
            info = string.Empty;
            //判断工单号是否存在
            if (string.IsNullOrEmpty(workOrder))
            {
                throw new ArgumentNullException("工号不能空");
            }
            try
            {                
                string sql = "select sfb01 from sfb_file where sfb01=:sfb01";
                OracleParameter p1 = new OracleParameter("sfb01", OracleDbType.Varchar2, 10);
                p1.Value = workOrder;
                object objRet = OracleHelper.ExecuteScalar(ERPConn, sql, p1);
                if (null == objRet || DBNull.Value.Equals(objRet))
                {
                    info = "工单号不存在";
                    return false;
                }
                else
                {
                    //判断是否发料
                    sql = "select sfa03 from sfa_file,sfb_file where sfa01=sfb01 and sfa05>0 and sfa06=0 and sfb02<>'15' and sfb39<>'2' and sfb01=:sfb01";
                    DataTable dt = OracleHelper.ExecuteDataTable(ERPConn, sql, p1);
                    if (!IsTableNull(dt))
                    {
                        StringBuilder sb = new StringBuilder(100);
                        sb.Append("工单:").Append(workOrder).Append(",以下料号未发料:\r");
                        foreach (DataRow dr in dt.Rows)
                        {
                            sb.Append(dr[0].ToString()).Append(",");
                        }
                        info = sb.ToString().TrimEnd(',');
                        return false;
                    }

                    //判断最后一道工序是否存在且转出数量大于0
                    sql = "select sfb93 from sfb_file where sfb01=:sfb01";
                    objRet = OracleHelper.ExecuteScalar(ERPConn, sql, p1);
                    if (!IsNullValue(objRet) && "Y" == objRet.ToString().ToUpper())
                    {
                        sql = "select ecm311 + ecm315 from ecm_file where ecm01=:sfb01 " +
                            " and ecm03=( select max(ecm03) from ecm_file where ecm01=:sfb01)";
                        int nRet = OracleHelper.ExecuteInt32(ERPConn, sql, p1);
                        if (nRet <= 0)
                        {
                            info = "工单:" + workOrder + "的最后一道工序无转出数量";
                            return false;
                        }
                    }
                    else
                    {
                        info = "无制程或制程不存在";
                        return false;
                    }

                    //验证料号信息
                    dt = GetMaterialCodeInfo(workOrder);
                    if (null == dt)
                    {
                        info = "未能找到该工单的料号";
                        return false;
                    }
                    else
                    {
                        string strTmp = string.Empty;
                        strTmp = dt.Rows[0][1].ToString() + "@";        //料号                
                        strTmp += dt.Rows[0][2].ToString() + "@";   //品名
                        strTmp += dt.Rows[0][3].ToString();   //规格
                        info = strTmp;
                        return true;
                    }
                }              
            }
            catch (System.Exception ex)
            {            
            	throw ex;
            }
            
        }

        [WebMethod(Description="通过工单号获取料号信息")]
        public DataTable GetMaterialCodeInfo(string workOrder)
        {
            try
            {
                string sql = "SELECT a.sfb01 工单号,b.ima01 料号,b.ima02 品名,b.ima021 规格 " +
                            " FROM sfb_file a ,ima_file b WHERE a.sfb05=b.ima01 and a.sfb01 = :sfb01";
                OracleParameter p1 = new OracleParameter("sfb01", OracleDbType.Varchar2, 10);
                p1.Value = workOrder;
                DataTable dt = OracleHelper.ExecuteDataTable(ERPConn, sql, p1);
                if (IsTableNull(dt))
                {
                    return null;
                }
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [WebMethod(Description="创建新条码")]
        public string CreateBarCode(string[] docHeader)
        {
            if (null == docHeader)
            {
                throw new ArgumentNullException("参数不能为空");
            }
            if (docHeader.Length < 8)
            {
                return null;
            }
            string  strRet = string.Empty;
            string serialNo,workOrder,BomNo,partName,specification,weekNo,quantity,depot,loc,remark;
            try
            {
                workOrder = docHeader[0];//工单号
                BomNo = docHeader[1];//料号
                partName = docHeader[2];//品名
                specification = docHeader[3];//规格
                weekNo = docHeader[4];//周番
                quantity = docHeader[5];//数量
                depot = docHeader[6];//仓库
                loc = docHeader[7];//储位
                remark = docHeader[8];//备注           
                serialNo = NewSerialNo(workOrder); //流水号
                String sql = "insert into jr_endproduct_file ( caseNumber ,productOrderId ,materialId ,materialName ,myStandard ,zhouFan ," +
                             "quantity ,orderId ,storageId ,locId  ,toStorageCode ,toLocCode ,operateDocNo ,status ,operateType ," +
                             "sourceDocNo ,remarks ,printDate ,printNumber ) values " +
                             "(:caseNumber ,:productOrderId ,:materialId ,:materialName ,myStandard ,zhouFan ," +
                             ":quantity ,'' ,:storageId ,:locId,'' ,'' ,'' ,'1' ,'0' ," +
                             "'',:remarks ,trunc(sysdate),1) ";
                OracleParameter[] parms = new OracleParameter[] 
                {
                    new OracleParameter("caseNumber",OracleDbType.Varchar2,20),
                    new OracleParameter("productOrderId",OracleDbType.Varchar2,50),
                    new OracleParameter("materialId",OracleDbType.Varchar2,20),
                    new OracleParameter("materialName",OracleDbType.Varchar2,30),
                    new OracleParameter("myStandard",OracleDbType.Varchar2,50),
                    new OracleParameter("zhouFan",OracleDbType.Varchar2,20),
                    new OracleParameter("quantity",OracleDbType.Decimal),
                    new OracleParameter("storageId",OracleDbType.Varchar2,20),
                    new OracleParameter("locId",OracleDbType.Varchar2,20),
                    new OracleParameter("remarks",OracleDbType.Varchar2,40),
                };
                parms[0].Value = serialNo;
                parms[1].Value = workOrder;
                parms[2].Value = BomNo;
                parms[3].Value = partName;
                parms[4].Value = specification;
                parms[5].Value = weekNo;
                parms[6].Value = quantity;
                parms[7].Value = depot;
                parms[8].Value = loc;
                parms[9].Value = remark;

                if (OracleHelper.ExecuteNonQuery(MESConn, CommandType.Text, sql, parms) > 0) //创建新条码      
                {
                    strRet = serialNo;
                }
                      

           }
           catch (System.Exception ex)
           {           	
           	throw ex;
           }
            return strRet;
        }
        #endregion





    }
}
