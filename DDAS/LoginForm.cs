﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DDASProject.CEUtility;
using System.Reflection;

namespace DDASProject.GZElite
{
    public partial class LoginForm : BaseForm
    {
        private string _userList;
        
        public LoginForm():base(AppID.Login)        
        {
            InitializeComponent();
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {
            InitFromData();
            //Common.HideTaskBar(this, false);           
        }

        private void InitFromData()
        {
            //加载用户名
           _userList = Common.GetGlobalSetting("UserList");
           if (!string.IsNullOrEmpty(_userList))
           {
               string[] userArr = _userList.Split(';');
               if (null != userArr)
               {
                   foreach (string s in userArr)
                   {
                       cbUserID.Items.Add(s);
                   }
                   cbUserID.SelectedIndex = 0;
               }
           }
        }
        private void tmrNow_Tick(object sender, EventArgs e)
        {
           statusBar.Text = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");
        }

        private void btnConfirm_Click(object sender, EventArgs e)
        {
            string user = cbUserID.Text;
            string password = tbPassword.Text;
            if (string.IsNullOrEmpty(user) || string.IsNullOrEmpty(password))
            {
                MessageBox.Show("用户名或密码不能为空","警告",MessageBoxButtons.OK,MessageBoxIcon.Exclamation,MessageBoxDefaultButton.Button1);
                return;
            }
            try
            {
                using(WaitCursor w = new WaitCursor())
                {
                    string userGroup = string.Empty;
                    if (WebSvr.Login(user, password, out userGroup))
                    {
                        if (ckbRememberUserID.Checked)
                        {
                            if (!string.IsNullOrEmpty(_userList) && !_userList.Contains(user))
                            {
                                _userList = user + ";" + _userList;
                                Common.SetGlobalSetting("UserList", _userList);
                            }
                        }
                        base._userID = user;
                        base._userGroup = userGroup;
                        this.DialogResult = DialogResult.Yes;

                    }
                    else
                    {
                        Common.PlayWarning();
                        MessageBox.Show("账户或密码错误!!", "提示");
                    }
                }
                
            }
            catch (System.Exception ex)
            {
            	Logger.Error(ex);
            	MessageBox.Show(ex.Message);            	
            }
               
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cbUserID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ('\r' == e.KeyChar)
            {
                tbPassword.Focus();
            }
        }

        private void tbPassword_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ('\r' == e.KeyChar)
            {
                btnConfirm_Click(sender, e);
                {
                    btnConfirm_Click(sender, e);
                }
            }
           
        }
    }
}

