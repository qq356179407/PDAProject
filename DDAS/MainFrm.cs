﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DDASProject.MaterialAllot;
using DDASProject.WebService;
using DDASProject.CEUtility;
using DDASProject.ElectrolyticPaper;
using System.Data.SqlServerCe;
using DDASProject.EndProductAllot;
namespace DDASProject.GZElite
{
    /**关于SQLCE数据库同步问题解决方案    
     *直接使用条码更新系统下载数据库即可
     **/
    public partial class MainFrm : BaseForm
    {
        
        #region 属性与变量
        private DataTable _authorityTable;  //权限列表
        #endregion
        #region 函数
        public MainFrm(string userID, string department)
            : base(AppID.FunctionFrm, userID, department)
        {
            InitializeComponent();
        }

        private void MainFrm_Load(object sender, EventArgs e)
        {
            try
            {
                lbInfo.Text = "正在加载中,请稍后.... ";
                InitFormData();
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
            }
            finally
            {
                lbInfo.Text = "";
            }
        }

        private void InitFormData()
        {
            try
            {
                _authorityTable = WebSvr.GetUserAuthority(UserID);
                foreach (DataRow dr in _authorityTable.Rows)
                {
                    ButtonAuthority(dr[0].ToString());
                }
            }
            catch (System.Exception ex)
            {
                Logger.Error(ex);
                throw ex;
            }           
        }

        /// <summary>
        /// 授权效验
        /// </summary>
        /// <param name="code">权限代码</param>
        private void ButtonAuthority(string code)
        {
            switch (code.Trim())
            {
                case "CL5": //材料调拨
                    this.btnMaterialAllot.Enabled = true;
                    break;
                case "CP5": //成品调拨
                    this.btnEndproductAllot.Enabled = true;
                    break;

                default:
                    break;
            }
        }

        /// <summary>
        /// 成品调拨
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEndproductAllot_Click(object sender, EventArgs e)
        {
            EndProductAllotFrm dlg = new EndProductAllotFrm(UserID, UserGroup);
            this.Hide();
            dlg.ShowDialog();
            this.Show();
        }

        /// <summary>
        /// 材料调拨
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnMaterialAllot_Click(object sender, EventArgs e)
        {
            MaterialAllotFrm dlg = new MaterialAllotFrm(UserID, UserGroup);
            this.Hide();
            dlg.ShowDialog();
            this.Show();
        }

        private void btnElectrolyticPaper_Click(object sender, EventArgs e)
        {
            ElectrolyticPaperFrm dlg = new ElectrolyticPaperFrm(UserID, UserGroup);
            this.Hide();
            dlg.ShowDialog();
            this.Show();
        }
        #endregion

    }
}