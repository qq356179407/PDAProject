﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Windows.Forms;
using DDASProject.CEUtility;
using System.IO;
using System.Reflection;

namespace DDASProject.GZElite
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [MTAThread]
        static void Main()
        {
            if (Common.IsExist())
            {
                MessageBox.Show("程序已运行", "警告", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                return;
            }
            try
            {
                //创建桌面快捷方式
                string file = @"\Windows\桌面\金日条码系统.lnk";
                if (!File.Exists(file))
                {
                    FileStream fileStream = File.Create(file);
                    StreamWriter sw = new StreamWriter(fileStream, System.Text.Encoding.Default);
                    string strContan = file.Length.ToString() + "#\"" + Assembly.GetExecutingAssembly().GetName().CodeBase + "\"";
                    sw.Write(strContan);
                    sw.Flush();
                    sw.Close();
                    fileStream.Close();
                }
            }
            catch (System.Exception ex)
            {
            	Common.Logger.Error(ex);            	
            }
            LoginForm dlg = new LoginForm();
           // Test dlg = new Test();
            //dlg.ShowDialog();
            if(DialogResult.Yes == dlg.ShowDialog())
               Application.Run(new MainFrm(dlg.UserID,dlg.UserGroup));
        }
    }
}