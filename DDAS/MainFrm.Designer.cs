﻿namespace DDASProject.GZElite
{
    partial class MainFrm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private new System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private new void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.page1 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbInfo = new System.Windows.Forms.Label();
            this.btnEndProductSet = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnPrintEndProductBarcode = new System.Windows.Forms.Button();
            this.btnEndProductArrearAllot = new System.Windows.Forms.Button();
            this.btnEndproductAllot = new System.Windows.Forms.Button();
            this.page2 = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnMaterialAllot = new System.Windows.Forms.Button();
            this.btnElectrolyticPaper = new System.Windows.Forms.Button();
            this.page3 = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnProductSlip = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.page1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.page2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.page3.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.page1);
            this.tabControl1.Controls.Add(this.page2);
            this.tabControl1.Controls.Add(this.page3);
            this.tabControl1.Font = new System.Drawing.Font("Tahoma", 10F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(238, 275);
            this.tabControl1.TabIndex = 0;
            // 
            // page1
            // 
            this.page1.Controls.Add(this.panel1);
            this.page1.Location = new System.Drawing.Point(4, 25);
            this.page1.Name = "page1";
            this.page1.Size = new System.Drawing.Size(230, 246);
            this.page1.Text = "成仓";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Info;
            this.panel1.Controls.Add(this.lbInfo);
            this.panel1.Controls.Add(this.btnEndProductSet);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.btnPrintEndProductBarcode);
            this.panel1.Controls.Add(this.btnEndProductArrearAllot);
            this.panel1.Controls.Add(this.btnEndproductAllot);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(230, 246);
            // 
            // lbInfo
            // 
            this.lbInfo.Location = new System.Drawing.Point(33, 119);
            this.lbInfo.Name = "lbInfo";
            this.lbInfo.Size = new System.Drawing.Size(160, 20);
            // 
            // btnEndProductSet
            // 
            this.btnEndProductSet.Enabled = false;
            this.btnEndProductSet.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.btnEndProductSet.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnEndProductSet.Location = new System.Drawing.Point(185, 3);
            this.btnEndProductSet.Name = "btnEndProductSet";
            this.btnEndProductSet.Size = new System.Drawing.Size(41, 30);
            this.btnEndProductSet.TabIndex = 0;
            this.btnEndProductSet.Text = "设置";
            this.btnEndProductSet.Click += new System.EventHandler(this.btnEndproductAllot_Click);
            // 
            // button1
            // 
            this.button1.Enabled = false;
            this.button1.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.button1.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.button1.Location = new System.Drawing.Point(107, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(80, 30);
            this.button1.TabIndex = 0;
            this.button1.Text = "备料调拨";
            this.button1.Click += new System.EventHandler(this.btnEndproductAllot_Click);
            // 
            // btnPrintEndProductBarcode
            // 
            this.btnPrintEndProductBarcode.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.btnPrintEndProductBarcode.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnPrintEndProductBarcode.Location = new System.Drawing.Point(109, 39);
            this.btnPrintEndProductBarcode.Name = "btnPrintEndProductBarcode";
            this.btnPrintEndProductBarcode.Size = new System.Drawing.Size(117, 30);
            this.btnPrintEndProductBarcode.TabIndex = 0;
            this.btnPrintEndProductBarcode.Text = "成品库存标签补打";
            this.btnPrintEndProductBarcode.Click += new System.EventHandler(this.btnEndproductAllot_Click);
            // 
            // btnEndProductArrearAllot
            // 
            this.btnEndProductArrearAllot.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.btnEndProductArrearAllot.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnEndProductArrearAllot.Location = new System.Drawing.Point(3, 39);
            this.btnEndProductArrearAllot.Name = "btnEndProductArrearAllot";
            this.btnEndProductArrearAllot.Size = new System.Drawing.Size(100, 30);
            this.btnEndProductArrearAllot.TabIndex = 0;
            this.btnEndProductArrearAllot.Text = "成品尾数调拨";
            this.btnEndProductArrearAllot.Click += new System.EventHandler(this.btnEndproductAllot_Click);
            // 
            // btnEndproductAllot
            // 
            this.btnEndproductAllot.Enabled = false;
            this.btnEndproductAllot.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.btnEndproductAllot.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnEndproductAllot.Location = new System.Drawing.Point(3, 3);
            this.btnEndproductAllot.Name = "btnEndproductAllot";
            this.btnEndproductAllot.Size = new System.Drawing.Size(100, 30);
            this.btnEndproductAllot.TabIndex = 0;
            this.btnEndproductAllot.Text = "成品调拨";
            this.btnEndproductAllot.Click += new System.EventHandler(this.btnEndproductAllot_Click);
            // 
            // page2
            // 
            this.page2.Controls.Add(this.panel2);
            this.page2.Location = new System.Drawing.Point(4, 25);
            this.page2.Name = "page2";
            this.page2.Size = new System.Drawing.Size(230, 246);
            this.page2.Text = "材仓";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Info;
            this.panel2.Controls.Add(this.btnMaterialAllot);
            this.panel2.Controls.Add(this.btnElectrolyticPaper);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(230, 246);
            // 
            // btnMaterialAllot
            // 
            this.btnMaterialAllot.Enabled = false;
            this.btnMaterialAllot.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.btnMaterialAllot.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnMaterialAllot.Location = new System.Drawing.Point(4, 3);
            this.btnMaterialAllot.Name = "btnMaterialAllot";
            this.btnMaterialAllot.Size = new System.Drawing.Size(80, 30);
            this.btnMaterialAllot.TabIndex = 2;
            this.btnMaterialAllot.Text = "材料调拨";
            // 
            // btnElectrolyticPaper
            // 
            this.btnElectrolyticPaper.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.btnElectrolyticPaper.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnElectrolyticPaper.Location = new System.Drawing.Point(4, 38);
            this.btnElectrolyticPaper.Name = "btnElectrolyticPaper";
            this.btnElectrolyticPaper.Size = new System.Drawing.Size(80, 30);
            this.btnElectrolyticPaper.TabIndex = 1;
            this.btnElectrolyticPaper.Text = "电解纸发料";
            this.btnElectrolyticPaper.Click += new System.EventHandler(this.btnElectrolyticPaper_Click);
            // 
            // page3
            // 
            this.page3.Controls.Add(this.panel3);
            this.page3.Location = new System.Drawing.Point(4, 25);
            this.page3.Name = "page3";
            this.page3.Size = new System.Drawing.Size(230, 246);
            this.page3.Text = "出货";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.Info;
            this.panel3.Controls.Add(this.btnProductSlip);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(230, 246);
            // 
            // btnProductSlip
            // 
            this.btnProductSlip.Enabled = false;
            this.btnProductSlip.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.btnProductSlip.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnProductSlip.Location = new System.Drawing.Point(3, 3);
            this.btnProductSlip.Name = "btnProductSlip";
            this.btnProductSlip.Size = new System.Drawing.Size(100, 30);
            this.btnProductSlip.TabIndex = 1;
            this.btnProductSlip.Text = "成品出货";
            // 
            // MainFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.SystemColors.Info;
            this.ClientSize = new System.Drawing.Size(238, 275);
            this.Controls.Add(this.tabControl1);
            this.MaximizeBox = false;
            this.Name = "MainFrm";
            this.Text = "功能选择";
            this.Load += new System.EventHandler(this.MainFrm_Load);
            this.tabControl1.ResumeLayout(false);
            this.page1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.page2.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.page3.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage page1;
        private System.Windows.Forms.TabPage page2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnElectrolyticPaper;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lbInfo;
        private System.Windows.Forms.Button btnEndproductAllot;
        private System.Windows.Forms.Button btnMaterialAllot;
        private System.Windows.Forms.Button btnEndProductSet;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btnEndProductArrearAllot;
        private System.Windows.Forms.TabPage page3;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnProductSlip;
        private System.Windows.Forms.Button btnPrintEndProductBarcode;

    }
}

