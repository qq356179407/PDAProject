﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace DDASProject.FTPClient
{
    public partial class frmRename : Form
    {
        private  string _newName;
        private string _oldName;
        public  string NewName
        {
            get { return _newName; }
        }
        public frmRename(string fileName)
        {
            InitializeComponent();
            _oldName = Path.GetFileName(fileName);
            tbNewName.Text = _oldName;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(tbNewName.Text))
            {
                MessageBox.Show("请输入文件名!");
                return;
            }
            _newName = tbNewName.Text;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void frmRename_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(tbNewName.Text))
            {
                tbNewName.Select(0, tbNewName.Text.IndexOf('.'));
            }            
        }
    }
}