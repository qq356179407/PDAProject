﻿namespace DDASProject.FTPClient
{
    partial class frmMain
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.menuItemA = new System.Windows.Forms.MenuItem();
            this.menuRelogin = new System.Windows.Forms.MenuItem();
            this.menuClose = new System.Windows.Forms.MenuItem();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.menuDLFile = new System.Windows.Forms.MenuItem();
            this.menuUPFile = new System.Windows.Forms.MenuItem();
            this.menuDelFile = new System.Windows.Forms.MenuItem();
            this.menuRename = new System.Windows.Forms.MenuItem();
            this.menuRefresh = new System.Windows.Forms.MenuItem();
            this.menuReturnRoot = new System.Windows.Forms.MenuItem();
            this.statusBar1 = new System.Windows.Forms.StatusBar();
            this.label1 = new System.Windows.Forms.Label();
            this.tbPath = new System.Windows.Forms.TextBox();
            this.btnGet = new System.Windows.Forms.Button();
            this.listFiles = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // mainMenu1
            // 
            this.mainMenu1.MenuItems.Add(this.menuItemA);
            this.mainMenu1.MenuItems.Add(this.menuItem1);
            // 
            // menuItemA
            // 
            this.menuItemA.MenuItems.Add(this.menuRelogin);
            this.menuItemA.MenuItems.Add(this.menuClose);
            this.menuItemA.Text = "系统";
            // 
            // menuRelogin
            // 
            this.menuRelogin.Text = "重新登陆";
            this.menuRelogin.Click += new System.EventHandler(this.menuRelogin_Click);
            // 
            // menuClose
            // 
            this.menuClose.Text = "退出";
            this.menuClose.Click += new System.EventHandler(this.menuClose_Click);
            // 
            // menuItem1
            // 
            this.menuItem1.MenuItems.Add(this.menuDLFile);
            this.menuItem1.MenuItems.Add(this.menuUPFile);
            this.menuItem1.MenuItems.Add(this.menuDelFile);
            this.menuItem1.MenuItems.Add(this.menuRename);
            this.menuItem1.MenuItems.Add(this.menuRefresh);
            this.menuItem1.MenuItems.Add(this.menuReturnRoot);
            this.menuItem1.Text = "操作";
            // 
            // menuDLFile
            // 
            this.menuDLFile.Text = "下载";
            this.menuDLFile.Click += new System.EventHandler(this.menuDLFile_Click);
            // 
            // menuUPFile
            // 
            this.menuUPFile.Text = "上传";
            this.menuUPFile.Click += new System.EventHandler(this.menuUPFile_Click);
            // 
            // menuDelFile
            // 
            this.menuDelFile.Text = "删除";
            this.menuDelFile.Click += new System.EventHandler(this.menuDelFile_Click);
            // 
            // menuRename
            // 
            this.menuRename.Text = "重命名";
            this.menuRename.Click += new System.EventHandler(this.menuRename_Click);
            // 
            // menuRefresh
            // 
            this.menuRefresh.Text = "获取目录";
            this.menuRefresh.Click += new System.EventHandler(this.menuRefresh_Click);
            // 
            // menuReturnRoot
            // 
            this.menuReturnRoot.Text = "返回根目录";
            this.menuReturnRoot.Click += new System.EventHandler(this.menuReturnRoot_Click);
            // 
            // statusBar1
            // 
            this.statusBar1.Font = new System.Drawing.Font("Tahoma", 11F, System.Drawing.FontStyle.Bold);
            this.statusBar1.Location = new System.Drawing.Point(0, 249);
            this.statusBar1.Name = "statusBar1";
            this.statusBar1.Size = new System.Drawing.Size(238, 26);
            this.statusBar1.Text = "状态:";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(4, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 17);
            this.label1.Text = "路径:";
            // 
            // tbPath
            // 
            this.tbPath.BackColor = System.Drawing.SystemColors.Info;
            this.tbPath.Location = new System.Drawing.Point(40, 29);
            this.tbPath.Name = "tbPath";
            this.tbPath.Size = new System.Drawing.Size(163, 23);
            this.tbPath.TabIndex = 2;
            // 
            // btnGet
            // 
            this.btnGet.Location = new System.Drawing.Point(207, 29);
            this.btnGet.Name = "btnGet";
            this.btnGet.Size = new System.Drawing.Size(28, 23);
            this.btnGet.TabIndex = 3;
            this.btnGet.Text = "转";
            this.btnGet.Click += new System.EventHandler(this.btnGet_Click);
            // 
            // listFiles
            // 
            this.listFiles.Columns.Add(this.columnHeader1);
            this.listFiles.Columns.Add(this.columnHeader2);
            this.listFiles.Columns.Add(this.columnHeader3);
            this.listFiles.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listFiles.Location = new System.Drawing.Point(1, 53);
            this.listFiles.Name = "listFiles";
            this.listFiles.Size = new System.Drawing.Size(235, 194);
            this.listFiles.TabIndex = 4;
            this.listFiles.View = System.Windows.Forms.View.Details;
            this.listFiles.SelectedIndexChanged += new System.EventHandler(this.listFiles_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "文件名";
            this.columnHeader1.Width = 100;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "大小";
            this.columnHeader2.Width = 60;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "日期";
            this.columnHeader3.Width = 130;
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.FileName = "noname";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "选择文件";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(238, 275);
            this.Controls.Add(this.listFiles);
            this.Controls.Add(this.btnGet);
            this.Controls.Add(this.tbPath);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.statusBar1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Menu = this.mainMenu1;
            this.Name = "frmMain";
            this.Text = "FTP客户端";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.MenuItem menuItemA;
        private System.Windows.Forms.MenuItem menuRelogin;
        private System.Windows.Forms.MenuItem menuClose;
        private System.Windows.Forms.StatusBar statusBar1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MenuItem menuItem1;
        private System.Windows.Forms.MenuItem menuDLFile;
        private System.Windows.Forms.MenuItem menuUPFile;
        private System.Windows.Forms.MenuItem menuRename;
        private System.Windows.Forms.MenuItem menuRefresh;
        private System.Windows.Forms.MenuItem menuDelFile;
        private System.Windows.Forms.TextBox tbPath;
        private System.Windows.Forms.Button btnGet;
        private System.Windows.Forms.ListView listFiles;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.MenuItem menuReturnRoot;
    }
}

