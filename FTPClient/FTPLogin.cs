﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using OpenNETCF.Net.Ftp;
using System.Text.RegularExpressions;
namespace DDASProject.FTPClient
{
    public partial class FTPLogin : Form
    {
        private string _userID;
        private string _password;
        private FTP _ftpClient;
       
        public string UserID
        {
            get { return _userID; }            
        }

        public FTP FtpClient
        {
            get { return _ftpClient; }            
        }

        public FTPLogin()
        {
            InitializeComponent();
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(tbUserID.Text))
            {
                MessageBox.Show("用户名不能为空");
                lbInfo.Text = "登陆失败,用户名为空!";
                return;
            }
            _userID = tbUserID.Text;
            _password = tbPassword.Text;
            LoginTest(_userID,_password);
            lbInfo.Text = "正在连接中……";    
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void textBox1_Validating(object sender, CancelEventArgs e)
        {
            //string strRegx = @"((?:(?:25[0-5]|2[0-4]\d|((1\d{2})|([1-9]?\d)))\.){3}(?:25[0-5]|2[0-4]\d|((1\d{2})|([1-9]?\d))))";
            //if (!Regex.IsMatch(tbServerIP.Text, strRegx))
            //{
            //    lbInfo.Text = "ＩＰ地址格式错误!";
            //    e.Cancel = true;
            //}
            //else
            //{
            //    lbInfo.Text = "";
            //}
            if (string.IsNullOrEmpty(tbServerIP.Text))
            {
                lbInfo.Text = "服务器地址不能为空";
                e.Cancel = true;
            }
            else
            {
                lbInfo.Text = "";
            }
        }

        private bool LoginTest(string userid, string pw)
        {
            bool bRet = false;
            try
            {              
                if (null == _ftpClient)
                {
                    _ftpClient = new FTP();
                }
                _ftpClient.ResponseReceived += new FTPResponseHandler(_ftpClient_ResponseReceived);
                _ftpClient.Connected += new FTPConnectedHandler(_ftpClient_Connected);
                _ftpClient.RemoteHost = tbServerIP.Text;
                _ftpClient.BeginConnect(userid, pw);
                bRet = _ftpClient.IsConnected;
            }
            catch (System.Exception ex)
            {
                lbInfo.Text = "登录错误!\r\n原因:" + ex.Message;
            }
            return bRet;
        }

        void _ftpClient_Connected(FTP source)
        {
            if (_ftpClient.IsConnected)
            {
                if (this.InvokeRequired)
                {
                    this.Invoke(new FTPConnectedHandler(_ftpClient_Connected), new object[] { source });
                } 
                else
                {
                    this.Hide();
                    frmMain dlg = new frmMain(_ftpClient);
                    DialogResult dlgResult = dlg.ShowDialog();
                    if (DialogResult.Retry == dlgResult)
                    {
                        tbPassword.Text = "";
                        tbUserID.Text = "";
                        lbInfo.Text = "";
                        _ftpClient.Disconnect();
                        _ftpClient.ResponseReceived -= new FTPResponseHandler(_ftpClient_ResponseReceived);
                        _ftpClient.Connected -= new FTPConnectedHandler(_ftpClient_Connected);
                        this.Show();
                    }
                    else
                    {
                        _ftpClient.ResponseReceived -= new FTPResponseHandler(_ftpClient_ResponseReceived);
                        _ftpClient.Connected -= new FTPConnectedHandler(_ftpClient_Connected);
                        _ftpClient.Disconnect();
                        this.Close();
                    }
                }
                
            }
            else
            {
                MessageBox.Show("用户名或密码错误", "登录失败");
            } 
            
        }

        void _ftpClient_ResponseReceived(FTP source, FTPResponse Response)
        {
            OnResponse(Response.Text);
        }

        delegate void StringDelegate(string value);
        private void OnResponse(string response)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new StringDelegate(OnResponse), new object[] { response });
                return;
            }
            //ListViewItem item = new ListViewItem(new string[] { DateTime.Now.ToShortTimeString(), response });
            //status.Items.Insert(0, item);
            //status.Columns[1].Width = -1;
            lbInfo.Text = response;
        }

        private void FTPLogin_KeyPress(object sender, KeyPressEventArgs e)
        {
            //if (e.KeyChar == '\r')
            //{
            //    btnOK_Click(sender, null);
            //}
        }

        private void tbServerIP_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar=='\r')
            {
                tbUserID.Focus();
            }
        }

        private void tbUserID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar=='\r')
            {
                tbPassword.Focus();
            }
        }

        private void tbPassword_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar=='\r')
            {
                btnOK_Click(sender, null);
            }
        }
    }
}