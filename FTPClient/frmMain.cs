﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using OpenNETCF.Net.Ftp;
using System.IO;
namespace DDASProject.FTPClient
{
    public partial class frmMain : Form
    {
        private FTP _ftpClient;
        private int _listCurrentIndex;
        public frmMain(FTP ftp)
        {
            InitializeComponent();
            _ftpClient = ftp;
            _listCurrentIndex = 0;
        }

        private void menuClose_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void menuRelogin_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Retry;
            this.Close();
        }

        //下载
        private void menuDLFile_Click(object sender, EventArgs e)
        {
            if (null == tbPath.Tag)
            {
                MessageBox.Show("请先获取目录!");
                return;
            }
            if (tbPath.Tag.ToString().Equals("DIR", StringComparison.OrdinalIgnoreCase))
            {
                MessageBox.Show("无法下载目录!");
                return;
            }
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                saveFileDialog1.FileName = tbPath.Text;
                if (DialogResult.OK == saveFileDialog1.ShowDialog())
                {
                    _ftpClient.GetFile(tbPath.Text, saveFileDialog1.FileName, true);
                }
                Cursor.Current = Cursors.Default;
            }
            catch (System.Exception ex)
            {
                statusBar1.Text = "错误:" + ex.Message;
            }
        }

        //上传
        private void menuUPFile_Click(object sender, EventArgs e)
        {
            if (DialogResult.OK == openFileDialog1.ShowDialog())
            {
                try
                {
                    Cursor.Current = Cursors.WaitCursor;
                    string fileName = openFileDialog1.FileName;
                    using(FileStream fileStream = new FileStream(fileName,FileMode.Open))
                    {
                        _ftpClient.SendFile(fileStream, Path.GetFileName(fileName));
                    }
                    Cursor.Current = Cursors.Default;
                }
                catch (System.Exception ex)
                {
                    statusBar1.Text = "错误:" + ex.Message;
                }
                
            }
        }

        //重命名
        private void menuRename_Click(object sender, EventArgs e)
        {
            if (_ftpClient.Username != "admin")
            {
                MessageBox.Show("您无权修改名称");
                return;
            }
            if (string.IsNullOrEmpty(tbPath.Text))
            {
                MessageBox.Show("请选择要重命名的文件!");
                return;
            }
            if (tbPath.Tag.ToString().Equals("DIR", StringComparison.OrdinalIgnoreCase))
            {
                MessageBox.Show("无法重命名的目录!");
                return;
            }
            try
            {
                frmRename dlg = new frmRename(tbPath.Text);
                if (DialogResult.OK == dlg.ShowDialog())
                {
                   string newFileName = dlg.NewName;
                   _ftpClient.RenameFile(tbPath.Text, newFileName);
                   tbPath.Text  = listFiles.Items[_listCurrentIndex].SubItems[0].Text = newFileName;
                   
                }
            }
            catch (System.Exception ex)
            {
                statusBar1.Text = "错误:" + ex.Message;
            }
        }

        //刷新
        private void menuRefresh_Click(object sender, EventArgs e)
        {
            if (null != _ftpClient && _ftpClient.IsConnected)
            {
                statusBar1.Text = "正在获取目录";
                Cursor.Current = Cursors.WaitCursor;
                FTPFiles files = _ftpClient.EnumFiles();

                listFiles.Items.Clear();
                listFiles.BeginUpdate();
                foreach (FTPFile file in files)
                {
                    float fileSize = file.Size/1024;
                    string strFileSize = "0";
                    if (fileSize < 1)
                    {
                        strFileSize = file.Size.ToString() + " byte";
                    } 
                    else
                    {
                        strFileSize = fileSize.ToString() + " kb";
                    }
                    if (file.Type == FTPFileType.Directory)
                    {
                        strFileSize = "目录";
                    } 
                    ListViewItem item = new ListViewItem(new string[] { file.Name, strFileSize, file.FileDate.ToString("yyyy-MM-dd hh:mm") });
                    item.Tag = file.Type;
                    listFiles.Items.Add(item);
                }
                listFiles.EndUpdate();
                statusBar1.Text = "完成";
                Cursor.Current = Cursors.Default;
            }
        }

        //删除
        private void menuDelFile_Click(object sender, EventArgs e)
        {
            if (_ftpClient.Username != "admin")
            {
                MessageBox.Show("您无权删除");
                return;
            }
            if (string.IsNullOrEmpty(tbPath.Text))
            {
                MessageBox.Show("请选择要删除文件!");
                return;
            }
            if (tbPath.Tag.ToString().Equals("DIR",StringComparison.OrdinalIgnoreCase))
            {
                MessageBox.Show("无法删除目录!");
                return;
            }
            try
            {
                if (DialogResult.Yes == MessageBox.Show("确认删除?","提示",MessageBoxButtons.YesNo,MessageBoxIcon.Asterisk,MessageBoxDefaultButton.Button1))
                {
                    _ftpClient.DeleteFile(tbPath.Text);
                    listFiles.Items.RemoveAt(_listCurrentIndex);
                    tbPath.Text = "";
                }
                
            }
            catch (System.Exception ex)
            {
                statusBar1.Text = "错误:" + ex.Message;
            }
            
        }

        //返回根目录
        private void menuReturnRoot_Click(object sender, EventArgs e)
        {
            tbPath.Text = "/";
            btnGet_Click(sender, e);
        }

        private void btnGet_Click(object sender, EventArgs e)
        {
            if (null == tbPath.Tag)
            {
                MessageBox.Show("请先获取目录");
                return;
            }
            bool isFolder = false;
            if (tbPath.Text.Equals("/") || tbPath.Text.Equals(".."))
            {
                isFolder = true;
            }
            Cursor.Current = Cursors.WaitCursor;
            if (isFolder || tbPath.Tag.ToString().Equals("DIR", StringComparison.OrdinalIgnoreCase))
            {
                try
                {
                    _ftpClient.ChangeDirectory(tbPath.Text);
                    menuRefresh_Click(sender, e);
                }
                catch (System.Exception ex)
                {
                    statusBar1.Text = "错误:" + ex.Message;
                }
               
            } 
            else
            {
                try
                {
                    saveFileDialog1.FileName = tbPath.Text;                    
                    if(DialogResult.OK == saveFileDialog1.ShowDialog())
                    {
                        _ftpClient.GetFile(tbPath.Text, saveFileDialog1.FileName, true);
                    }
                }
                catch (System.Exception ex)
                {
                    statusBar1.Text = "错误:" + ex.Message;
                }
            }
            Cursor.Current = Cursors.Default;
        }

        private void listFiles_SelectedIndexChanged(object sender, EventArgs e)
        {
            System.Windows.Forms.ListView.SelectedIndexCollection selectItems = this.listFiles.SelectedIndices;
            if (selectItems.Count > 0)
            {

                if ((FTPFileType)this.listFiles.Items[selectItems[0]].Tag == FTPFileType.Directory)
                {                    
                    tbPath.Tag = "DIR";
                }
                else
                {                   
                    tbPath.Tag = "FILE";
                }
                this.tbPath.Text = this.listFiles.Items[selectItems[0]].SubItems[0].Text.Trim();
                this._listCurrentIndex = this.listFiles.Items[selectItems[0]].Index;
            } 
        }


    }
}