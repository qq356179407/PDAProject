﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DDASProject.ElectrolyticPaper
{
    public partial class InfoFrm : DDASProject.ExtentLib.GridForm
    {
       
        public InfoFrm(DataTable dt):base(dt)
        {
            InitializeComponent();
            
        }
        protected override void InitDataGrid()
        {
            try
            {
                if (null != _dataTable && null != _dataTable.Rows || _dataTable.Rows.Count > 0)
                {
                    DataTable dt = new DataTable("newTable");
                    DataColumn col1 = new DataColumn("料号");
                    dt.Columns.Add(col1);
                    DataColumn col2 = new DataColumn("总计数量");
                    dt.Columns.Add(col2);
                    dt.PrimaryKey = new DataColumn[] { col1 };
                    foreach (DataRow row in _dataTable.Rows)
                    {
                        object obj1 = row["料号"];
                        object obj2 = row["数量"];
                        double quantity = Convert.ToDouble(obj2);
                        if (!dt.Rows.Contains(obj1))
                        {
                            DataRow dr = dt.NewRow();
                            dr["料号"] = obj1;
                            dr["总计数量"] = quantity;
                            dt.Rows.Add(dr);
                        }
                        else
                        {
                            DataRow dr = dt.Rows.Find(obj1);
                            dr["总计数量"] = Convert.ToDouble(dr["总计数量"]) + quantity;
                        }

                    }

                    _dataTable = dt;
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            base.InitDataGrid();
        }
    }
}

