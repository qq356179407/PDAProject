﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DDASProject.CEUtility;

namespace DDASProject.ElectrolyticPaper
{
    public partial class ElectrolyticPaperFrm : DDASProject.CEUtility.BaseForm
    {
        private DataTable dtSource;
        private uint _index;
        public ElectrolyticPaperFrm(String userid,string usergroup):
            base(AppID.ElectrolyticPaper,userid, usergroup)   
        {
            InitializeComponent();
            InitFormData();
            HasNewVersion();
        }

        
        private void InitFormData()
        {
            if (null == dtSource)
            {
                dtSource = new DataTable("paperTable");
            }
     
            DataColumn column1 = new DataColumn("项次");
            DataColumn column2 = new DataColumn("箱号");
            DataColumn column3 = new DataColumn("料号");
            DataColumn column4 = new DataColumn("批号");
            DataColumn column5 = new DataColumn("数量");
            DataColumn column6 = new DataColumn("单位");
            DataColumn column7 = new DataColumn("供应商");
            DataColumn column8 = new DataColumn("面积");
            DataColumn column9 = new DataColumn("容积");
            DataColumn column10 = new DataColumn("打印日期");
            DataColumn column11 = new DataColumn("备注");
            
            dtSource.Columns.Add(column1);
            dtSource.Columns.Add(column2);
            dtSource.Columns.Add(column3);
            dtSource.Columns.Add(column4);
            dtSource.Columns.Add(column5);
            dtSource.Columns.Add(column6);
            dtSource.Columns.Add(column7);
            dtSource.Columns.Add(column8);
            dtSource.Columns.Add(column9);
            dtSource.Columns.Add(column10);
            dtSource.Columns.Add(column11);  
            dtSource.PrimaryKey = new System.Data.DataColumn[] { column2 }; //主键
            dataGrid.DataSource = dtSource;
            _index = 0;
        }

        private void tbBillCode_Validating(object sender, CancelEventArgs e)
        {
            if (3 == tbBillCode.Text.Length && BillCodeVaild(tbBillCode.Text))
            {
                e.Cancel = false;
            }
            else
            {
                MessageBox.Show("无效单别!", "提示");
                e.Cancel = true;
            }
        }

        private void tbBillCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                tbBarCode.Focus();
            }
        }

        private void tbBarCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                IsBarCodeVaild(tbBarCode.Text);
            }
        }

        private void IsBarCodeVaild(string txtBarcode)
        {
            if (string.IsNullOrEmpty(txtBarcode))
            {
                MessageBox.Show("条码不能为空", "错误");
                tbBarCode.Focus();
                return;
            }
            if (string.IsNullOrEmpty(tbBillCode.Text))
            {
                MessageBox.Show("单别不能为空", "错误");
                tbBillCode.Focus();
                return;
            }
            
            if (!BarcodeVaild(2, txtBarcode, 0))
            {
                tbBillCode.SelectAll();
                return;
            }
            lbInfo.Text = "";
            try
            {
                string materialNo = string.Empty; //料号
                string supplierNo = string.Empty; //供应商编号
                string lotNo = string.Empty;  //批号
                string quantity = string.Empty;  //数量
                string unit = string.Empty;  //库存单位
                string area = string.Empty;  //面积
                string cap = string.Empty;  //容量
                string serialNo = string.Empty;  //流水号
                string printDate = string.Empty;  //打印日期
                string remark = string.Empty;  //备注
                string[] arr = txtBarcode.Split('&');
                if (arr.Length < 11)
                {
                    throw new FormatException("条码解析异常");
                }

                materialNo = arr[1];
                supplierNo = arr[2];
                lotNo = arr[3];
                quantity = arr[4];
                unit = arr[5];
                area = arr[6];
                cap = arr[7];
                serialNo = arr[8];
                printDate = arr[9];
                remark = arr[10];


                if (dtSource.Rows.Count > 0 && dtSource.Rows.Contains(serialNo))
                {
                    Common.PlayWarning();
                    lbInfo.Text = "警告:条码已扫描!";
                    tbBarCode.Text = "";
                    tbBarCode.Focus();
                    return;
                }
             
                DataRow newRow = dtSource.NewRow();
                newRow[0] = ++_index;   //项次
                newRow[1] = serialNo;   //箱号
                newRow[2] = materialNo; //料号
                newRow[3] = lotNo;      //批号
                newRow[4] = quantity;  //数量
                newRow[5] = unit;      //单位
                newRow[6] = supplierNo;      //供应商
                newRow[7] = area;      //面积
                newRow[8] = cap;      //容积
                newRow[9] = printDate;      //打印日期
                newRow[10] = remark;      //备注

                dtSource.Rows.Add(newRow);
                btnSum.Text = "共" + dtSource.Rows.Count.ToString() + "箱";
                tbBarCode.Text = string.Empty;
            }
            catch (Exception ex)
            {
                Common.PlayWarning();
                Logger.Error(ex);
                lbInfo.Text = ex.Message;
                tbBarCode.Focus();
            }
        }
        
        //生单
        private void btnGenerateBill_Click(object sender, EventArgs e)
        {
            try
            {
                string docNo = string.Empty;
                string[] docHead = new string[] {tbBillCode.Text,UserID,UserGroup };
                if (null == dtSource || dtSource.Rows.Count <= 0)
                {
                    MessageBox.Show("没有可上传的数据!");
                    return;
                }
                ErrorCode retCode = (ErrorCode)WebSvr.CreateAllotDocument(2, docHead, dtSource, false, out docNo);
                if (retCode != ErrorCode.ERR_NONE)
                {
                    MessageBox.Show(Error.GetErrorMsg(retCode),"错误");
                }
                else
                {
                    MessageBox.Show("生单成功,单号:"+docNo,"提示");
                    lbInfo.Text = "单号:" + docNo;
                    dtSource.Clear();
                }
            }
            catch (System.Exception ex)
            {
            	Logger.Error(ex);
            	lbInfo.Text =  ex.Message;            	
            }
        }

        private void btnDel_Click(object sender, EventArgs e)
        {
            if (null != dtSource &&  dtSource.Rows.Count > 0)
            {
                //gridTable
                int currentIndex = dataGrid.CurrentRowIndex;
                if (currentIndex >= 0 && DialogResult.Yes == MessageBox.Show("确认删除", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1))
                {
                    dtSource.Rows.RemoveAt(currentIndex);
                }
            }
        }

        private void btnSum_Click(object sender, EventArgs e)
        {
            InfoFrm dlg = new InfoFrm(_dtSource);
            dlg.ShowDialog();            
        }
    }
}

