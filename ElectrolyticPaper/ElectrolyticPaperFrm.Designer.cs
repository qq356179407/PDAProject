﻿namespace DDASProject.ElectrolyticPaper
{
    partial class ElectrolyticPaperFrm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private new System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private new void InitializeComponent()
        {
            this.lbInfo = new System.Windows.Forms.Label();
            this.btnSum = new System.Windows.Forms.Button();
            this.btnDel = new System.Windows.Forms.Button();
            this.btnGenerateBill = new System.Windows.Forms.Button();
            this.dataGrid = new System.Windows.Forms.DataGrid();
            this.tbBarCode = new System.Windows.Forms.TextBox();
            this.tbBillCode = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbInfo
            // 
            this.lbInfo.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.lbInfo.ForeColor = System.Drawing.Color.Red;
            this.lbInfo.Location = new System.Drawing.Point(5, 220);
            this.lbInfo.Name = "lbInfo";
            this.lbInfo.Size = new System.Drawing.Size(227, 18);
            // 
            // btnSum
            // 
            this.btnSum.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.btnSum.Location = new System.Drawing.Point(75, 241);
            this.btnSum.Name = "btnSum";
            this.btnSum.Size = new System.Drawing.Size(79, 24);
            this.btnSum.TabIndex = 10;
            this.btnSum.Text = "共(0)箱";
            this.btnSum.Click += new System.EventHandler(this.btnSum_Click);
            // 
            // btnDel
            // 
            this.btnDel.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.btnDel.Location = new System.Drawing.Point(5, 241);
            this.btnDel.Name = "btnDel";
            this.btnDel.Size = new System.Drawing.Size(46, 24);
            this.btnDel.TabIndex = 9;
            this.btnDel.Text = "删除";
            this.btnDel.Click += new System.EventHandler(this.btnDel_Click);
            // 
            // btnGenerateBill
            // 
            this.btnGenerateBill.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.btnGenerateBill.Location = new System.Drawing.Point(182, 241);
            this.btnGenerateBill.Name = "btnGenerateBill";
            this.btnGenerateBill.Size = new System.Drawing.Size(46, 24);
            this.btnGenerateBill.TabIndex = 11;
            this.btnGenerateBill.Text = "生单";
            this.btnGenerateBill.Click += new System.EventHandler(this.btnGenerateBill_Click);
            // 
            // dataGrid
            // 
            this.dataGrid.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dataGrid.Location = new System.Drawing.Point(3, 69);
            this.dataGrid.Name = "dataGrid";
            this.dataGrid.Size = new System.Drawing.Size(232, 148);
            this.dataGrid.TabIndex = 12;
            // 
            // tbBarCode
            // 
            this.tbBarCode.Location = new System.Drawing.Point(54, 38);
            this.tbBarCode.Name = "tbBarCode";
            this.tbBarCode.Size = new System.Drawing.Size(174, 23);
            this.tbBarCode.TabIndex = 8;
            this.tbBarCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbBarCode_KeyPress);
            // 
            // tbBillCode
            // 
            this.tbBillCode.Location = new System.Drawing.Point(54, 9);
            this.tbBillCode.Name = "tbBillCode";
            this.tbBillCode.Size = new System.Drawing.Size(174, 23);
            this.tbBillCode.TabIndex = 6;
            this.tbBillCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbBillCode_KeyPress);
            this.tbBillCode.Validating += new System.ComponentModel.CancelEventHandler(this.tbBillCode_Validating);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(8, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 20);
            this.label2.Text = "条码:";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(8, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 20);
            this.label1.Text = "单别:";
            // 
            // ElectrolyticPaperFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(238, 275);
            this.Controls.Add(this.lbInfo);
            this.Controls.Add(this.btnSum);
            this.Controls.Add(this.btnDel);
            this.Controls.Add(this.btnGenerateBill);
            this.Controls.Add(this.dataGrid);
            this.Controls.Add(this.tbBarCode);
            this.Controls.Add(this.tbBillCode);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ElectrolyticPaperFrm";
            this.Text = "电解纸发料";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lbInfo;
        private System.Windows.Forms.Button btnSum;
        private System.Windows.Forms.Button btnDel;
        private System.Windows.Forms.Button btnGenerateBill;
        private System.Windows.Forms.DataGrid dataGrid;
        private System.Windows.Forms.TextBox tbBarCode;
        private System.Windows.Forms.TextBox tbBillCode;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}
