﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.Net;

namespace DDASProject.ExtentLib
{
    public static class PrintHelper
    {


        /// <summary>
        /// 列印成品条码内容命令
        /// </summary>
        /// <param name="endProduct">成品列印内容</param>
        /// <returns></returns>
        public static string GetPrintContent(stEndproduct endProduct)
        {
            StringBuilder sb = new StringBuilder(200);
            string strLine = "\r\n";
            sb.Append(strLine);
            string sn = string.Format("&{0}&{1}&{2}&{3}&{4}&{5}&",endProduct.boxNo,endProduct.materialNo,endProduct.lotNo,endProduct.weekNo,
                                        endProduct.quantity, endProduct.remark);
            //string sn = "&" + endProduct.boxNo + "&" + endProduct.materialNo + "&" + endProduct.lotNo + "&" + endProduct.weekNo + "&"
            //    + endProduct.quantity + "&" + endProduct.remark + "&";
            sb.Append("! 0 200 200 240 1");
            sb.Append(strLine);
            sb.Append("LABEL");
            sb.Append(strLine);
            sb.Append("CONTRAST 0");
            sb.Append(strLine);
            sb.Append("TONE 0");
            sb.Append(strLine);
            sb.Append("SPEED 3");
            sb.Append(strLine);
            sb.Append("PAGE-WIDTH 560");
            sb.Append(strLine);
            sb.Append("BAR-SENSE");
            sb.Append(strLine);
            sb.Append("COUNTRY CHINA");
            sb.Append(strLine);
            sb.Append(";// PAGE 0000000005600240");
            sb.Append(strLine);
            sb.Append("B PDF-417 232 57 XD 2 YD 5 C 2 S 1");
            sb.Append(strLine);
            sb.Append(sn);
            sb.Append(strLine);
            sb.Append("ENDPDF");
            sb.Append(strLine);
            sb.Append("T 7 0 11 12 P/N: ");
            sb.Append(strLine);
            sb.Append("T 7 0 10 45 L/N: ");
            sb.Append(strLine);
            sb.Append("T 7 0 10 77 D/C: ");
            sb.Append(endProduct.weekNo);
            sb.Append(strLine);
            sb.Append("T 7 0 10 109 QTY: ");
            sb.Append(endProduct.quantity);
            sb.Append(strLine);
            sb.Append("T 7 0 10 141 S/N: ");
            sb.Append(endProduct.boxNo);
            sb.Append(strLine);
            sb.Append("T 7 0 8 173 RMKS: ");
            sb.Append(endProduct.remark);
            sb.Append(strLine);
            sb.Append("FORM");
            sb.Append(strLine);
            sb.Append("PRINT");
            sb.Append(strLine);
            return sb.ToString();
        }

        /// <summary>
        /// 发送指令
        /// </summary>
        /// <param name="ipAddress">对方IP地址</param>
        /// <param name="port">端口</param>
        /// <param name="commandText">命令内容</param>
        public static void SendCommand(IPAddress ipAddress, int port, string commandText)
        {
            Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            IPEndPoint point = new IPEndPoint(ipAddress, port);
            try
            {
                socket.Connect(point);//因为客户端只是用来向特定的服务器发送信息，所以不需要绑定本机的IP和端口。不需要监听。
                socket.Send(Encoding.Default.GetBytes(commandText)); //发送消息
                socket.Shutdown(SocketShutdown.Both);//关闭端口
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            finally
            {
                socket.Close();
            }
        }
    }
}
