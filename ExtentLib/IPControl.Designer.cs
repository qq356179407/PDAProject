﻿namespace DDASProject.ExtentLib
{
    partial class IPControl
    {
        /// <summary> 
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 组件设计器生成的代码

        /// <summary> 
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.field1 = new DDASProject.ExtentLib.FieldControl();
            this.field2 = new DDASProject.ExtentLib.FieldControl();
            this.field3 = new DDASProject.ExtentLib.FieldControl();
            this.field4 = new DDASProject.ExtentLib.FieldControl();
            this.tbPort = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(28, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(10, 27);
            this.label1.Text = ".";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(65, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(10, 27);
            this.label2.Text = ".";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(100, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(10, 27);
            this.label3.Text = ".";
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.SystemColors.HighlightText;
            this.label4.Location = new System.Drawing.Point(140, 2);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(10, 27);
            this.label4.Text = ":";
            // 
            // field1
            // 
            this.field1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.field1.FieldIndex = -1;
            this.field1.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.field1.Location = new System.Drawing.Point(3, 2);
            this.field1.MaxLength = 3;
            this.field1.Name = "field1";
            this.field1.RangeLower = ((byte)(0));
            this.field1.RangeUpper = ((byte)(255));
            this.field1.Size = new System.Drawing.Size(25, 23);
            this.field1.TabIndex = 6;
            this.field1.TabStop = false;
            this.field1.Text = "192";
            this.field1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // field2
            // 
            this.field2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.field2.FieldIndex = -1;
            this.field2.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.field2.Location = new System.Drawing.Point(37, 2);
            this.field2.MaxLength = 3;
            this.field2.Name = "field2";
            this.field2.RangeLower = ((byte)(0));
            this.field2.RangeUpper = ((byte)(255));
            this.field2.Size = new System.Drawing.Size(25, 23);
            this.field2.TabIndex = 6;
            this.field2.TabStop = false;
            this.field2.Text = "168";
            this.field2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // field3
            // 
            this.field3.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.field3.FieldIndex = -1;
            this.field3.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.field3.Location = new System.Drawing.Point(74, 2);
            this.field3.MaxLength = 3;
            this.field3.Name = "field3";
            this.field3.RangeLower = ((byte)(0));
            this.field3.RangeUpper = ((byte)(255));
            this.field3.Size = new System.Drawing.Size(25, 23);
            this.field3.TabIndex = 6;
            this.field3.TabStop = false;
            this.field3.Text = "0";
            this.field3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // field4
            // 
            this.field4.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.field4.FieldIndex = -1;
            this.field4.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.field4.Location = new System.Drawing.Point(113, 2);
            this.field4.MaxLength = 3;
            this.field4.Name = "field4";
            this.field4.RangeLower = ((byte)(0));
            this.field4.RangeUpper = ((byte)(255));
            this.field4.Size = new System.Drawing.Size(25, 23);
            this.field4.TabIndex = 6;
            this.field4.TabStop = false;
            this.field4.Text = "30";
            this.field4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tbPort
            // 
            this.tbPort.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbPort.Font = new System.Drawing.Font("Tahoma", 10F, System.Drawing.FontStyle.Bold);
            this.tbPort.Location = new System.Drawing.Point(150, 2);
            this.tbPort.MaxLength = 5;
            this.tbPort.Name = "tbPort";
            this.tbPort.Size = new System.Drawing.Size(38, 23);
            this.tbPort.TabIndex = 4;
            this.tbPort.Text = "6101";
            this.tbPort.Validated += new System.EventHandler(this.tbPort_Validated);
            this.tbPort.Validating += new System.ComponentModel.CancelEventHandler(this.tbPort_Validating);
            // 
            // IPControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Controls.Add(this.field4);
            this.Controls.Add(this.field3);
            this.Controls.Add(this.field2);
            this.Controls.Add(this.field1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbPort);
            this.Controls.Add(this.label1);
            this.Name = "IPControl";
            this.Size = new System.Drawing.Size(188, 23);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private FieldControl field1;
        private FieldControl field2;
        private FieldControl field3;
        private FieldControl field4;
        private System.Windows.Forms.TextBox tbPort;
    }
}
