﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DDASProject.ExtentLib
{
    //public class BarcodeInfo
    //{
    //}
    //物料条码信息
    [Serializable]
    public struct stMaterial
    {
        //&料号&供应商编号&批号&数量&库存单位&面积&容量&流水号&打印日期&备注&[仓库&储位]
        public string materialNo; //料号
        public string supplierNo; //供应商编号
        public string lotNo;  //批号
        public string quantity;  //数量
        public string unit;  //库存单位
        public string area;  //面积
        public string cap;  //容量
        public string serialNo;  //流水号
        public string printDate;  //打印日期
        public string remark;  //备注
        public string depotID;//仓库
        public string locID;//储位
       
        public stMaterial(string[] arr)
        {
            if (null == arr || arr.Length < 10)
            {
                throw new ArgumentException("参数不能为空或数组长度不正确");
            }
            materialNo = arr[1];
            supplierNo = arr[2];
            lotNo = arr[3];
            quantity = arr[4];
            unit = arr[5];
            area = arr[6];
            cap = arr[7];
            serialNo = arr[8];
            printDate = arr[9];
            remark = arr[10];
            depotID = string.Empty;
            locID = " ";
            if (arr.Length >= 12)
            {
                depotID = arr[11];                
            }
            if (arr.Length >= 13)
            {
                locID = string.IsNullOrEmpty(arr[12]) ? " " : arr[12];
            }
        }
    }
    //成品条码信息
    [Serializable]
    public struct stEndproduct
    {
        //&箱号&料号&批号/工单号&周番&数量&备注[&仓库&储位]
        public string boxNo;  //箱号
        public string materialNo;  //料号
        public string lotNo;  //批号
        public string weekNo;  //周番
        public string quantity;  //数量
        public string remark;  //备注
        public string depotID;//仓库
        public string locID;//储位
        public stEndproduct(string[] arr)
        {
            if (null == arr || arr.Length < 6)
            {
                throw new ArgumentException("参数不能为空或数组长度不正确");
            }
            boxNo = arr[1];
            materialNo = arr[2];
            lotNo = arr[3];
            weekNo = arr[4];
            quantity = arr[5];
            remark = arr[6];
            depotID = string.Empty;
            locID = string.Empty;
            if (arr.Length >= 8)
            {
                depotID = arr[7];                
            }
            if (arr.Length >= 9)
            {
                locID = arr[8];
            }
        }
    }
}
