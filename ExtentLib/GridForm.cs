﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace DDASProject.ExtentLib
{
    public partial class GridForm : Form
    {
        protected DataTable _dataTable;
        public GridForm() : this(new DataTable("newTable"))
        {
            
        }
        public GridForm(DataTable dt)
        {
            this.Size = new System.Drawing.Size(240, 300);
            InitializeComponent();
            _dataTable = dt;                    
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected virtual void InitDataGrid()
        {
            if (null != _dataTable)
            {
                this.dataGrid.DataSource = _dataTable;                
            }
        }
    }
}