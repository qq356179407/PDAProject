﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DDASProject.ExtentLib;

namespace DDASProject.MaterialAllot
{
    public partial class InfoFrm : GridForm
    {
        public InfoFrm(DataTable dt) : base(dt)
        {
            InitializeComponent();
            _dataTable = dt;            
        }

        protected override void InitDataGrid()
        {
            if (null != _dataTable && _dataTable.Rows.Count > 0)
            {
                DataTable dt = new DataTable();
                DataColumn column1 = new DataColumn("料号");
                DataColumn column2 = new DataColumn("数量");
                DataColumn column3 = new DataColumn("面积");
                dt.Columns.Add(column1);
                dt.Columns.Add(column2);
                dt.Columns.Add(column3);
                foreach (DataRow dr in _dataTable.Rows)
                {
                    DataRow newRow = dt.NewRow();
                    newRow[0] = dr["料号"];
                    newRow[1] = dr["数量"];
                    newRow[2] = dr["面积"];
                    dt.Rows.Add(newRow);
                }
                base.InitDataGrid();
            }
        }

        private void InfoFrm_Load(object sender, EventArgs e)
        {
            InitDataGrid();
        }
    }
}

