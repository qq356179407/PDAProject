﻿namespace DDASProject.MaterialAllot
{
    partial class MaterialAllotFrm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private new System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private new void InitializeComponent()
        {
            this.btnDel = new System.Windows.Forms.Button();
            this.btnSum = new System.Windows.Forms.Button();
            this.btnGenerateBill = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tbOutDepot = new System.Windows.Forms.TextBox();
            this.tbInDepot = new System.Windows.Forms.TextBox();
            this.tbBillCode = new System.Windows.Forms.TextBox();
            this.tbBarCode = new System.Windows.Forms.TextBox();
            this.cbAllotType = new System.Windows.Forms.ComboBox();
            this.cbReasonCode = new System.Windows.Forms.ComboBox();
            this.dataGrid = new System.Windows.Forms.DataGrid();
            this.btnClear = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnDel
            // 
            this.btnDel.Location = new System.Drawing.Point(9, 249);
            this.btnDel.Name = "btnDel";
            this.btnDel.Size = new System.Drawing.Size(56, 20);
            this.btnDel.TabIndex = 7;
            this.btnDel.Text = "删除";
            this.btnDel.Click += new System.EventHandler(this.btnDel_Click);
            // 
            // btnSum
            // 
            this.btnSum.Location = new System.Drawing.Point(76, 249);
            this.btnSum.Name = "btnSum";
            this.btnSum.Size = new System.Drawing.Size(72, 20);
            this.btnSum.TabIndex = 6;
            this.btnSum.Text = "共0箱";
            this.btnSum.Click += new System.EventHandler(this.btnSum_Click);
            // 
            // btnGenerateBill
            // 
            this.btnGenerateBill.Location = new System.Drawing.Point(159, 249);
            this.btnGenerateBill.Name = "btnGenerateBill";
            this.btnGenerateBill.Size = new System.Drawing.Size(72, 20);
            this.btnGenerateBill.TabIndex = 8;
            this.btnGenerateBill.Text = "生单";
            this.btnGenerateBill.Click += new System.EventHandler(this.btnGenerateBill_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(3, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 20);
            this.label1.Text = "调出仓";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(16, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 20);
            this.label2.Text = "单别";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            this.label3.Location = new System.Drawing.Point(1, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 16);
            this.label3.Text = "调拨性质";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(5, 92);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 20);
            this.label4.Text = "条码";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(119, 10);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 18);
            this.label5.Text = "调入仓";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(119, 39);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 19);
            this.label6.Text = "理由码";
            // 
            // tbOutDepot
            // 
            this.tbOutDepot.Location = new System.Drawing.Point(57, 7);
            this.tbOutDepot.Name = "tbOutDepot";
            this.tbOutDepot.Size = new System.Drawing.Size(59, 23);
            this.tbOutDepot.TabIndex = 0;
            this.tbOutDepot.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbOutDepot_KeyPress);
            this.tbOutDepot.Validating += new System.ComponentModel.CancelEventHandler(this.tbOutDepot_Validating);
            // 
            // tbInDepot
            // 
            this.tbInDepot.Location = new System.Drawing.Point(173, 7);
            this.tbInDepot.Name = "tbInDepot";
            this.tbInDepot.Size = new System.Drawing.Size(59, 23);
            this.tbInDepot.TabIndex = 1;
            this.tbInDepot.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbInDepot_KeyPress);
            this.tbInDepot.Validating += new System.ComponentModel.CancelEventHandler(this.tbInDepot_Validating);
            // 
            // tbBillCode
            // 
            this.tbBillCode.Enabled = false;
            this.tbBillCode.Location = new System.Drawing.Point(57, 35);
            this.tbBillCode.Name = "tbBillCode";
            this.tbBillCode.Size = new System.Drawing.Size(59, 23);
            this.tbBillCode.TabIndex = 2;
            this.tbBillCode.Text = "430";
            this.tbBillCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbBillCode_KeyPress);
            this.tbBillCode.Validating += new System.ComponentModel.CancelEventHandler(this.tbBillCode_Validating);
            // 
            // tbBarCode
            // 
            this.tbBarCode.Location = new System.Drawing.Point(41, 89);
            this.tbBarCode.Name = "tbBarCode";
            this.tbBarCode.Size = new System.Drawing.Size(175, 23);
            this.tbBarCode.TabIndex = 5;
            this.tbBarCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbBarCode_KeyPress);
            // 
            // cbAllotType
            // 
            this.cbAllotType.BackColor = System.Drawing.SystemColors.Info;
            this.cbAllotType.Items.Add("1 一阶段仓库调拨 ");
            this.cbAllotType.Items.Add("2 二阶段仓库调拨");
            this.cbAllotType.Items.Add("5 零库存调出");
            this.cbAllotType.Items.Add("6 零库存调入");
            this.cbAllotType.Location = new System.Drawing.Point(57, 62);
            this.cbAllotType.Name = "cbAllotType";
            this.cbAllotType.Size = new System.Drawing.Size(175, 23);
            this.cbAllotType.TabIndex = 4;
            this.cbAllotType.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbAllotType_KeyPress);
            // 
            // cbReasonCode
            // 
            this.cbReasonCode.BackColor = System.Drawing.SystemColors.Info;
            this.cbReasonCode.Items.Add("1001");
            this.cbReasonCode.Items.Add("1002");
            this.cbReasonCode.Items.Add("1003");
            this.cbReasonCode.Items.Add("1006");
            this.cbReasonCode.Items.Add("1008");
            this.cbReasonCode.Items.Add("1009");
            this.cbReasonCode.Items.Add("1010");
            this.cbReasonCode.Location = new System.Drawing.Point(173, 36);
            this.cbReasonCode.Name = "cbReasonCode";
            this.cbReasonCode.Size = new System.Drawing.Size(59, 23);
            this.cbReasonCode.TabIndex = 3;
            this.cbReasonCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbReasonCode_KeyPress);
            // 
            // dataGrid
            // 
            this.dataGrid.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.dataGrid.Location = new System.Drawing.Point(4, 115);
            this.dataGrid.Name = "dataGrid";
            this.dataGrid.Size = new System.Drawing.Size(230, 129);
            this.dataGrid.TabIndex = 11;
            // 
            // btnClear
            // 
            this.btnClear.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnClear.BackColor = System.Drawing.Color.Transparent;
            this.btnClear.Font = new System.Drawing.Font("Tahoma", 8F, System.Drawing.FontStyle.Regular);
            this.btnClear.ForeColor = System.Drawing.Color.DarkGoldenrod;
            this.btnClear.Location = new System.Drawing.Point(217, 90);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(15, 20);
            this.btnClear.TabIndex = 7;
            this.btnClear.Text = "清";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // MaterialAllotFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.ClientSize = new System.Drawing.Size(238, 275);
            this.Controls.Add(this.dataGrid);
            this.Controls.Add(this.cbAllotType);
            this.Controls.Add(this.tbOutDepot);
            this.Controls.Add(this.cbReasonCode);
            this.Controls.Add(this.tbInDepot);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbBillCode);
            this.Controls.Add(this.tbBarCode);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnDel);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnSum);
            this.Controls.Add(this.btnGenerateBill);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MaterialAllotFrm";
            this.Text = "物料调拨";
            this.Load += new System.EventHandler(this.MaterialAllotFrm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnDel;
        private System.Windows.Forms.Button btnSum;
        private System.Windows.Forms.Button btnGenerateBill;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tbOutDepot;
        private System.Windows.Forms.TextBox tbInDepot;
        private System.Windows.Forms.TextBox tbBillCode;
        private System.Windows.Forms.TextBox tbBarCode;
        private System.Windows.Forms.ComboBox cbAllotType;
        private System.Windows.Forms.ComboBox cbReasonCode;
        private System.Windows.Forms.DataGrid dataGrid;
        private System.Windows.Forms.Button btnClear;
    }
}
