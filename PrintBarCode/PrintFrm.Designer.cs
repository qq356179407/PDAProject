﻿namespace DDASProject.PrintBarCode
{
    partial class PrintFrm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PrintFrm));
            this.label1 = new System.Windows.Forms.Label();
            this.ipPrintAddress = new DDASProject.ExtentLib.IPControl();
            this.label13 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.cbDepot = new System.Windows.Forms.ComboBox();
            this.tbWorkOrder = new System.Windows.Forms.TextBox();
            this.tbBomNo = new System.Windows.Forms.TextBox();
            this.tbPartName = new System.Windows.Forms.TextBox();
            this.tbSpecification = new System.Windows.Forms.TextBox();
            this.tbWeekNo = new System.Windows.Forms.TextBox();
            this.tbRemark = new System.Windows.Forms.TextBox();
            this.numericEveryBoxQuantity = new System.Windows.Forms.NumericUpDown();
            this.tbLoc = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(4, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 20);
            this.label1.Text = "IP:";
            // 
            // ipPrintAddress
            // 
            this.ipPrintAddress.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ipPrintAddress.IPAddStruct = new byte[] {
        ((byte)(127)),
        ((byte)(0)),
        ((byte)(0)),
        ((byte)(1))};
            this.ipPrintAddress.Location = new System.Drawing.Point(34, 4);
            this.ipPrintAddress.Name = "ipPrintAddress";
            this.ipPrintAddress.Size = new System.Drawing.Size(188, 23);
            this.ipPrintAddress.TabIndex = 0;
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(140, 209);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(37, 20);
            this.label13.Text = "储号";
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(8, 180);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 20);
            this.label8.Text = "备注";
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(8, 107);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 20);
            this.label7.Text = "规格";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(8, 82);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 20);
            this.label6.Text = "品名";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(8, 58);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 20);
            this.label5.Text = "料号";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(7, 210);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 20);
            this.label4.Text = "仓号";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(6, 156);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 20);
            this.label3.Text = "单箱数量";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(8, 133);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 20);
            this.label2.Text = "周番";
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(8, 34);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(60, 20);
            this.label9.Text = "工单号";
            // 
            // btnPrint
            // 
            this.btnPrint.Location = new System.Drawing.Point(15, 243);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(72, 20);
            this.btnPrint.TabIndex = 7;
            this.btnPrint.Text = "打印";
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(130, 243);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(72, 20);
            this.btnClose.TabIndex = 8;
            this.btnClose.Text = "关闭";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // cbDepot
            // 
            this.cbDepot.Items.Add("C000");
            this.cbDepot.Items.Add("C003");
            this.cbDepot.Items.Add("D000");
            this.cbDepot.Items.Add("D008");
            this.cbDepot.Location = new System.Drawing.Point(53, 207);
            this.cbDepot.Name = "cbDepot";
            this.cbDepot.Size = new System.Drawing.Size(77, 23);
            this.cbDepot.TabIndex = 4;
            this.cbDepot.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cbDepot_KeyPress);
            // 
            // tbWorkOrder
            // 
            this.tbWorkOrder.Location = new System.Drawing.Point(72, 32);
            this.tbWorkOrder.Name = "tbWorkOrder";
            this.tbWorkOrder.Size = new System.Drawing.Size(150, 23);
            this.tbWorkOrder.TabIndex = 1;
            this.tbWorkOrder.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbWorkOrder_KeyPress);
            // 
            // tbBomNo
            // 
            this.tbBomNo.BackColor = System.Drawing.SystemColors.ControlLight;
            this.tbBomNo.Enabled = false;
            this.tbBomNo.Location = new System.Drawing.Point(72, 56);
            this.tbBomNo.Name = "tbBomNo";
            this.tbBomNo.Size = new System.Drawing.Size(150, 23);
            this.tbBomNo.TabIndex = 22;
            // 
            // tbPartName
            // 
            this.tbPartName.BackColor = System.Drawing.SystemColors.ControlLight;
            this.tbPartName.Enabled = false;
            this.tbPartName.Location = new System.Drawing.Point(72, 80);
            this.tbPartName.Name = "tbPartName";
            this.tbPartName.Size = new System.Drawing.Size(150, 23);
            this.tbPartName.TabIndex = 22;
            // 
            // tbSpecification
            // 
            this.tbSpecification.BackColor = System.Drawing.SystemColors.ControlLight;
            this.tbSpecification.Enabled = false;
            this.tbSpecification.Location = new System.Drawing.Point(72, 104);
            this.tbSpecification.Name = "tbSpecification";
            this.tbSpecification.Size = new System.Drawing.Size(150, 23);
            this.tbSpecification.TabIndex = 22;
            // 
            // tbWeekNo
            // 
            this.tbWeekNo.Location = new System.Drawing.Point(72, 128);
            this.tbWeekNo.Name = "tbWeekNo";
            this.tbWeekNo.Size = new System.Drawing.Size(150, 23);
            this.tbWeekNo.TabIndex = 2;
            this.tbWeekNo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbWeekNo_KeyPress);
            // 
            // tbRemark
            // 
            this.tbRemark.Location = new System.Drawing.Point(72, 178);
            this.tbRemark.Name = "tbRemark";
            this.tbRemark.Size = new System.Drawing.Size(150, 23);
            this.tbRemark.TabIndex = 6;
            this.tbRemark.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbRemark_KeyPress);
            // 
            // numericEveryBoxQuantity
            // 
            this.numericEveryBoxQuantity.Location = new System.Drawing.Point(72, 152);
            this.numericEveryBoxQuantity.Maximum = new decimal(new int[] {
            9999,
            0,
            0,
            0});
            this.numericEveryBoxQuantity.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericEveryBoxQuantity.Name = "numericEveryBoxQuantity";
            this.numericEveryBoxQuantity.Size = new System.Drawing.Size(150, 24);
            this.numericEveryBoxQuantity.TabIndex = 3;
            this.numericEveryBoxQuantity.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericEveryBoxQuantity.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numericEveryBoxQuantity_KeyPress);
            // 
            // tbLoc
            // 
            this.tbLoc.Location = new System.Drawing.Point(178, 205);
            this.tbLoc.Name = "tbLoc";
            this.tbLoc.Size = new System.Drawing.Size(44, 23);
            this.tbLoc.TabIndex = 5;
            this.tbLoc.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbLoc_KeyPress);
            // 
            // PrintFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(238, 275);
            this.Controls.Add(this.numericEveryBoxQuantity);
            this.Controls.Add(this.tbRemark);
            this.Controls.Add(this.tbLoc);
            this.Controls.Add(this.tbWeekNo);
            this.Controls.Add(this.tbSpecification);
            this.Controls.Add(this.tbPartName);
            this.Controls.Add(this.tbBomNo);
            this.Controls.Add(this.tbWorkOrder);
            this.Controls.Add(this.cbDepot);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnPrint);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.ipPrintAddress);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PrintFrm";
            this.Text = "标签打印";
            this.Load += new System.EventHandler(this.PrintFrm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private DDASProject.ExtentLib.IPControl ipPrintAddress;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.ComboBox cbDepot;
        private System.Windows.Forms.TextBox tbWorkOrder;
        private System.Windows.Forms.TextBox tbBomNo;
        private System.Windows.Forms.TextBox tbPartName;
        private System.Windows.Forms.TextBox tbSpecification;
        private System.Windows.Forms.TextBox tbWeekNo;
        private System.Windows.Forms.TextBox tbRemark;
        private System.Windows.Forms.NumericUpDown numericEveryBoxQuantity;
        private System.Windows.Forms.TextBox tbLoc;
    }
}