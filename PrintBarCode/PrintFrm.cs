﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DDASProject.ExtentLib;

namespace DDASProject.PrintBarCode
{
    public partial class PrintFrm : Form
    {
        public PrintFrm()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        //打印
        private void btnPrint_Click(object sender, EventArgs e)
        {
            
            if (DialogResult.Yes == MessageBox.Show("确认打印?","提示",MessageBoxButtons.YesNo,MessageBoxIcon.Asterisk,MessageBoxDefaultButton.Button1))
            {
                System.Net.IPAddress ip = ipPrintAddress.IpAddr;
                int port = ipPrintAddress.Port;
              
                if (string.IsNullOrEmpty(tbWorkOrder.Text))
                {
                    MessageBox.Show("工单号不能为空");
                    tbWorkOrder.Focus();
                    return;
                }
               
                if (string.IsNullOrEmpty(tbBomNo.Text))
                {
                    MessageBox.Show("料号为空,无效的工单号");
                    tbBomNo.Focus();
                    return;
                }
                
                if (string.IsNullOrEmpty(tbWeekNo.Text))
                {
                    MessageBox.Show("周蕃不能为空");
                    tbWeekNo.Focus();
                    return;
                }
                
                //工单号,料号,品名,规格,周番,数量,仓库,储位,备注
                string[] arr = new string[] {tbWorkOrder.Text,tbBomNo.Text,tbPartName.Text,tbSpecification.Text,
                    tbWeekNo.Text,numericEveryBoxQuantity.Value.ToString(),cbDepot.Text,tbLoc.Text,tbRemark.Text };
                try
                {
                    string strRet = CEUtility.Common.WebSvr.CreateBarCode(arr);
                    if (!string.IsNullOrEmpty(strRet))
                    {
                        //&箱号&料号&批号/工单号&周番&数量&备注
                        stEndproduct endproductInfo = new stEndproduct(new string[] { strRet,tbBomNo.Text,tbWorkOrder.Text,tbWeekNo.Text,numericEveryBoxQuantity.Value.ToString(),tbRemark.Text});
                        string printContent = PrintHelper.GetPrintContent(endproductInfo);
                        PrintHelper.SendCommand(ip, port, printContent);  //发送打印指令
                    }
                    else
                    {
                        MessageBox.Show("打印失败,无法创建箱号");
                    }
                }
                catch (System.Exception ex)
                {
                	CEUtility.Common.Logger.Error(ex);
                	MessageBox.Show(ex.Message);
                }
                

            }
        }

        private void PrintFrm_Load(object sender, EventArgs e)
        {
            
        }

  
        private void tbWorkOrder_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar=='\r'&& !string.IsNullOrEmpty(tbWorkOrder.Text))
            {
                //效验
                string retInfo = string.Empty;
                try
                {
                    if (CEUtility.Common.WebSvr.WorkOrderVaild(tbWorkOrder.Text, out retInfo))
                    {
                        if (!string.IsNullOrEmpty(retInfo))
                        {
                            string[] infoArr = retInfo.Split('@');
                            if (infoArr.Length > 2)
                            {
                                tbBomNo.Text = infoArr[0];
                                tbPartName.Text = infoArr[1];
                                tbSpecification.Text = infoArr[2];
                                tbWeekNo.Focus();
                            }
                            else
                            {
                                MessageBox.Show("料号异常,或者解析错误","错误");
                            }
                        }                        
                    }
                    else
                    {
                        MessageBox.Show(retInfo, "提示");
                    }
                }
                catch (System.Exception ex)
                {
                	CEUtility.Common.Logger.Error(ex);
                	MessageBox.Show(ex.ToString());
                }
                
            }
        }


        private void tbWeekNo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar=='\r')
            {
                numericEveryBoxQuantity.Focus();
            }
        }

        private void numericEveryBoxQuantity_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar=='\r')
            {
                tbRemark.Focus();
            }
        }

        private void cbDepot_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                tbLoc.Focus();
            }
        }

        private void tbRemark_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar=='\r')
            {
                cbDepot.Focus();
            }
        }

        private void tbLoc_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                btnPrint.Focus();
            }
        }
    }
}