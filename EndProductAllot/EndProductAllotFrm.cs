﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DDASProject.CEUtility;

namespace DDASProject.EndProductAllot
{
    public partial class EndProductAllotFrm : DDASProject.CEUtility.BaseForm
    {
        #region  属性与变量
        private DataTable dtSource;
        #endregion
        public EndProductAllotFrm(string userid,string usergroup)
            : base(AppID.EndproductAllot, userid, usergroup)    
        {
            InitializeComponent();            
        }

        private void EndProductAllotFrm_Load(object sender, EventArgs e)
        {
            HasNewVersion(); //版本检测
        }

        //生单
        private void btnGenerateBill_Click(object sender, EventArgs e)
        {

        }

        //统计箱数
        private void btnSum_Click(object sender, EventArgs e)
        {
            InfoFrm dlg = new InfoFrm(dtSource);
            dlg.ShowDialog();   
        }

        //删除
        private void btnDel_Click(object sender, EventArgs e)
        {
            int index = dataGrid.CurrentRowIndex;
            if (index > 0 || DialogResult.Yes == MessageBox.Show("确认删除?", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1))
            {
                try
                {
                    dtSource.Rows.RemoveAt(index);
                    btnSum.Text = "共" + dtSource.Rows.Count.ToString() + "箱";
                    if (dtSource.Rows.Count <= 0)
                    {
                        tbBillCode.Enabled = true;
                    }
                }
                catch (System.Exception ex)
                {
                    Logger.Error(ex);
                    MessageBox.Show(ex.Message);
                }

            }
        }

        //清空条码内容
        private void btnClear_Click(object sender, EventArgs e)
        {
            tbBarCode.Text = string.Empty;
            tbBarCode.Focus();
        }

      
    }
}

